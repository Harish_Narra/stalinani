//
//  AppDelegate.swift
//  IPAC
//
//  Created by Admin on 10/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import TwitterCore
import TwitterKit
import UserNotifications
import IQKeyboardManagerSwift
import SwiftyJSON
import EZSwiftExtensions
import GoogleMaps
import Fabric
//import Crashlytics
import Firebase

enum loginController {
    case fromLoginWithOTP
    case fromResetScreen
}

let sharedAppDelegate = UIApplication.shared.delegate as! AppDelegate
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var isUserLoggedIn = false
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
      //  LocalizationSystem.sharedInstance.setLanguage(languageCode: "ta")

        
       // GoogleLoginController.shared.configure(withClientId: "575545718899-4bn3k55kr9k7h5gl85p05hbi9h3hmp45.apps.googleusercontent.com")
         GoogleLoginController.shared.configure(withClientId: "25488029003-en2v5tj9v0l21aa5pu0cis9sns29k12k.apps.googleusercontent.com")
        IQKeyboardManager.shared.enable = true
        Fabric.with([Crashlytics.self])
        TWTRTwitter.sharedInstance().start(withConsumerKey: SocialKeys.Twitter.K_CONSUMER_KEY, consumerSecret: SocialKeys.Twitter.K_CONSUMER_SECRET)
        AWSController.setupAmazonS3(withPoolID: "us-east-1:b1f250f2-66a7-4d07-96e9-01817149a439")
       // GMSServices.provideAPIKey("AIzaSyBAKKgtwImMaN-REQEWPfSrMNL45g1I3ug")
        GMSServices.provideAPIKey("AIzaSyDWHysoJ2OlZFSeo2qV_ZS9M1vXV0UYigg")
        LocationManager.shared.updateLocation()
        Messaging.messaging().delegate = self
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
            
        }

        FirebaseApp.configure()

        setupPush(application)
        return true
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        print_debug(url.scheme ?? "")
        if url.scheme == "twitterkit-mztgufzuop3hk6uy19wjod1qc" {
            return TWTRTwitter.sharedInstance().application(application, open: url, options: options)
        } else if (url.scheme == "fb2160827270800884") {
            return ApplicationDelegate.shared.application(application, open: url, options: options)
        }  else if url.scheme?.lowercased() == "ipac" {
            let userData = url.absoluteString.components(separatedBy: "=")
            let value = url.absoluteString.components(separatedBy: "userId=")
            let val = value[1]
            let id = val.components(separatedBy: "&")
            print_debug(userData.last)
            print_debug(id.first)
            CommonFunction.gotoForgotController(useId: id.first ?? "", phoneNo: userData.last ?? "")
        }
        return GoogleLoginController.shared.handleUrl(url, options: options)
        //return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "yyyy-MM-dd HH:mm:ss"
        AppUserDefaults.save(value: dateFormatter.string(from: Date()) , forKey: .date)
       // Messaging.messaging().shouldEstablishDirectChannel = false

    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if application.applicationIconBadgeNumber > 0 {
            application.applicationIconBadgeNumber = 0
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if CommonFunction.isRemembered() == false{
            CommonFunction.removeUser()
        }
    }
    
    func setupPush(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }

    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        debugPrint(notification.request.content.userInfo)
        completionHandler([UNNotificationPresentationOptions.alert,
                           UNNotificationPresentationOptions.sound,
                           UNNotificationPresentationOptions.badge])
        let userInfo  = JSON(notification.request.content.userInfo)
        let pushModel = PushNotification.init(dict: JSON(userInfo["aps"].dictionaryValue))
        if pushModel.data.type == "5"{
        let vc = BonusReceivedVC.instantiate(fromAppStoryboard: .Form)
        vc.descriptionStr = pushModel.alert.body
        vc.titleStr = pushModel.alert.title
            guard let user = CommonFunction.getUser() else {return}
            user.is_bonus_received = "1"
            CommonFunction.updateUser(user)
        ez.topMostVC?.presentVC(vc)
        }
        
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(fcmToken)")

      let dataDict:[String: String] = ["token": fcmToken]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print_debug("Failed to register for remote notifications with error: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken

        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print_debug(token)

        DeviceDetail.deviceToken = token
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        print("Message ID: \(userInfo["gcm.message_id"]!)")
//        completionHandler(UIBackgroundFetchResult.newData)
        
        if let messageID = userInfo["gcm.message_id"] {
          print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)

        completionHandler(UIBackgroundFetchResult.newData)
    }
 
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print_debug("userNotificationCenter didReceive called")
        let userInfo = response.notification.request.content.userInfo
        print_debug(userInfo)
        if UIApplication.shared.applicationState == .active{
            handleNotifcation(userInfo:JSON.init(userInfo))

        }else{
                self.handleNotifcation(userInfo:JSON.init(userInfo))
        }
        completionHandler()

        }
    }
    

//MARK::- PUSH TAP ACTIONS
extension AppDelegate{
    func handleNotifcation(userInfo : JSON) {
        if CommonFunction.getUser() == nil {return}
        let pushModel = PushNotification.init(dict: JSON(userInfo["aps"].dictionaryValue))
        let type = pushModel.data.type
        switch type{
        case "2","3": //gotohome
            DeviceDetail.pushType = type.toInt() ?? -1

            CommonFunction.gotoHome()
            break
        case "4":
            CommonFunction.gotoHomeWithProfile()
            break
            
        case "5":
            ez.runThisAfterDelay(seconds: 2.0, after: {
                let vc = BonusReceivedVC.instantiate(fromAppStoryboard: .Form)
                vc.descriptionStr = pushModel.alert.body
                vc.titleStr = pushModel.alert.title
                guard let user = CommonFunction.getUser() else {return}
                if user.is_bonus_received == "1"{return}
                user.is_bonus_received = "1"
                CommonFunction.updateUser(user)
                ez.topMostVC?.presentVC(vc)
            })
    
        default :
            break
        }
    }
}
