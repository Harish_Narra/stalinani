//
//  LocationCell.swift
//  IPAC
//
//  Created by Appinventiv on 13/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    
    @IBOutlet weak var editTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var dropDownBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonFunction.setAttributesto(currentTextField: editTextField)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUserInfo(user : UserModel?,isLocation : Bool,idProofType : String){
        if isLocation {
            self.editTextField.text = user?.stateName
        }
        else{
            self.editTextField.text = idProofType
        }
    }
   
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.editTextField.text = ""
    }
    
}
