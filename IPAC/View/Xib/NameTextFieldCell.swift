//
//  NameTextFieldCell.swift
//  IPAC
//
//  Created by Appinventiv on 13/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class NameTextFieldCell: UITableViewCell {

    
    @IBOutlet weak var editInfoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.emailErrorLabel.isHidden = true
        CommonFunction.setAttributesto(currentTextField: editInfoTextField)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUserInfo(user : UserModel?,index : IndexPath)
    {
        switch index.section {
            case 0:
                switch index.row{
                case 1:
                    editInfoTextField.text = user?.registerationNo
                case 2:
                    editInfoTextField.text = user?.emailId
                    if user?.emailApproved == "1"{
                        self.emailErrorLabel.isHidden = true
                    } else {
                        self.emailErrorLabel.isHidden = true
                    }
                case 3:
                    editInfoTextField.text = user?.state

                case 4:
                    editInfoTextField.text = user?.district
                default:
                    print_debug("default")
                }
            break
            
        default:
            self.emailErrorLabel.isHidden = true
            editInfoTextField.isUserInteractionEnabled = false

            switch index.row {
                case 0:
                    editInfoTextField.text = user?.fbUsername == "" ? "-" : user?.fbUsername
                break
                case 1:
                    editInfoTextField.text = user?.twitterUsername == "" ? "-" : user?.twitterUsername
                break
                case 2:
                    editInfoTextField.text = user?.phoneNumber
                break
                case 3:
                    editInfoTextField.text = user?.whatsupNumber
                    editInfoTextField.isUserInteractionEnabled = true
                break
                default:
                     editInfoTextField.text = user?.paytmNumber
                break
                }
            }
        
    }
    
}
