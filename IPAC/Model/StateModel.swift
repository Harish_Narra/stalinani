//
//  StateModel.swift
//  IPAC
//
//  Created by macOS on 22/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import  SwiftyJSON

struct StateModel {
    
    let stateId: String
    let stateName: String
    
    init(dict: JSON) {
        stateId = dict["state_id"].stringValue
        stateName = dict["state_name"].stringValue
    }
}


