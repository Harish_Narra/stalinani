//
//  ReferralModel.swift
//  IPAC
//
//  Created by Appinventiv on 17/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

//
//class MainSubclass : NSObject{
//    
//    var achievement : [MainAchievement]!
//    var cODE : Int!
//    var levels : [MainLevel]!
//    var mESSAGE : String!
//    var totalWalletAmount : String!
//    var uSERINFO : MainUSERINFO!
//    var valid : Bool!
//    var wALLETDETAIL : MainWALLETDETAIL!
//    
//    /**
//     * Instantiate the instance using the passed json values to set the properties values
//     */
//    init(fromJson json: JSON!){
//        if json.isEmpty{
//            return
//        }
//        achievement = [MainAchievement]()
//        let achievementArray = json["achievement"].arrayValue
//        for achievementJson in achievementArray{
//            let value = MainAchievement(fromJson: custArrayJson)
//            custArray.append(value)
//        }
//        cODE = json["CODE"].intValue
//        levels = [MainLevel]()
//        let levelsArray = json["levels"].arrayValue
//        for levelsJson in levelsArray{
//            let value = MainLevel(fromJson: custArrayJson)
//            custArray.append(value)
//        }
//        mESSAGE = json["MESSAGE"].stringValue
//        totalWalletAmount = json["total_wallet_amount"].stringValue
//        let uSERINFOJson = json["USERINFO"]
//        if !uSERINFOJson.isEmpty{
//            uSERINFO = MainUSERINFO(fromJson: uSERINFOJson)
//        }
//        valid = json["valid"].boolValue
//        let wALLETDETAILJson = json["WALLET_DETAIL"]
//        if !wALLETDETAILJson.isEmpty{
//            wALLETDETAIL = MainWALLETDETAIL(fromJson: wALLETDETAILJson)
//        }
//    }
//    
//    /**
//     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
//     */
//   
//    
//}



class OfflineTaskModel : NSObject{
    var message : String?
    var count : Int?
    var tasks : [TaskModel]?

    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.message = json["MESSAGE"].stringValue
        self.count = json["COUNT"].intValue
        if let results = json["RESULT"].array { self.tasks = results.map { TaskModel(json: $0) } }
    }
}

class TaskModel : NSObject{
    
    var taskId : String?
    var taskTitle : String?
    
    override init() {
        super.init()
    }
    
    required init(json : JSON) {
        self.taskId = json["task_id"].stringValue
        self.taskTitle = json["task_title"].stringValue

    }
}





class ReferralModel : NSObject{
    
    var totalReferral: String?
    var message : String?
    var count : Int?
    var userInfo : UserInfo?
    var walletDetail : WalletDetail?

    var code : Int?
    var result : [ReferralResult]?

    
    override init() {
        super.init()
    }
    
    required  init(json: JSON) {
        
        self.totalReferral = json["TOTAL_REFERAL"].stringValue
        self.message = json["MESSAGE"].stringValue
        self.count = json["COUNT"].intValue
        let userInfo = json["USERINFO"].dictionaryValue
        self.userInfo = UserInfo.init(dict: JSON(userInfo))
        self.code = json["CODE"].intValue
        let walletDetails = json["WALLET_DETAIL"].dictionaryValue
        self.walletDetail = WalletDetail.init(dict: JSON(walletDetails))
        if let results = json["RESULT"].array { self.result = results.map { ReferralResult(json: $0) } }
    }
    
    static func getArr(jsonArr: JSON)->[ReferralModel]{
        var tempArr : [ReferralModel] = []
        for item in jsonArr.arrayValue {
            let ob = ReferralModel.init(json: item)
            tempArr.append(ob)
        }
        return (tempArr)
    }
}

class ReferralResult : NSObject{
    
    var email_id : String?
    var full_name : String?
    var registeration_no : String?
    var phone_number : String?
    var user_id : String?
    var user_image : String?
    
    //wallet
    var status : String?
    var created_date : String?
    var descriptions : String?
    var title : String?
    var task_id : String?
    var wallet_id : String?
    var point : String?
    var type : String?
    
    
    override init() {
        super.init()
    }
    
    required init(json : JSON) {
        email_id = json["email_id"].stringValue
        full_name = json["full_name"].stringValue
        registeration_no = json["registeration_no"].stringValue
        phone_number = json["phone_number"].stringValue
        user_id = json["user_id"].stringValue
        user_image = json["user_image"].stringValue
        created_date = json["created_date"].stringValue
        
        //wallet
        status = json["status"].stringValue
        created_date = json["created_date"].stringValue
        descriptions = json["description"].stringValue
        title = json["title"].stringValue
        task_id = json["task_id"].stringValue
        wallet_id = json["wallet_id"].stringValue
        point = json["point"].stringValue
        type = json["type"].stringValue

    }
    static func getArr(jsonArr: JSON)->[ReferralResult]{
        var tempArr : [ReferralResult] = []
        for item in jsonArr.arrayValue {
            let ob = ReferralResult.init(json: item)
            tempArr.append(ob)
        }
        return (tempArr)
    }
}

struct AppVersion{
    let version_name : String
    let update_type : String
    
    init(dict : JSON) {
        self.version_name = dict["version_name"].stringValue
        self.update_type = dict["update_type"].stringValue

    }
}
