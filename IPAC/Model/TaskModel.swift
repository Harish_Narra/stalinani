//
//  TaskModel.swift
//  IPAC
//
//  Created by macOS on 18/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

enum TaskType: String{
    
    case none
    case twitter
    case facebook
    case online
    case offline
    case youtube
    case whatsapp
    
    init(rawValue: String){
        
        switch rawValue {
        case "twitter":
            self = .twitter
        case "facebook":
            self = .facebook
        case "online":
            self = .online
        case "offline":
            self = .offline
        case "youtube":
            self = .youtube
        case "whatsapp":
            self = .whatsapp
        default:
            self = .none
        }
    }
}

enum TaskAction: String{
    
    case none = "none"
    case like
    case share
    case tweet
    case retweet
    case event
    case qrcode = "qr code"
    case aboutUs = "about us"
    case createGroup = "create group"
    case follow
    case post
    case setdp = "set dp"
    case link
    case image
    case video
    case subscribe
    case comment
    case form
    case downLoadMedia = "download media"
    
    init(rawValue: String){
        
        switch rawValue {
        case "like":
            self = .like
        case "share":
            self = .share
        case "post":
            self = .post
        case "tweet":
            self = .tweet
        case "retweet":
            self = .retweet
        case "set dp":
            self = .setdp
        case "link":
            self = .link
        case "image":
            self = .image
        case "video":
            self = .video
        case "event":
            self = .event
        case "qrcode" :
            self = .qrcode
        case "about us":
            self = .aboutUs
        case "create group":
            self = .createGroup
        case "follow":
            self = .follow
        case "subscribe":
            self = .subscribe
        case "comment":
            self = .comment
        case "form":
            self = .form
        case "download media":
            self = .downLoadMedia
   
        default:
            self = .none
        }
    }
}

struct TaskTabModel {
    
    let text:String
    let district: String
    let action: TaskAction
    let state: String
    let start_date: String
    let registeration_no: String
    let pc: String
    let created_date: String
    let gender: String
    let task_title: String
    let task_description: String
    let college: String
    let task_type: TaskType
    let ac: String
    let task_status: String
    let post_id: String
    let task_id: String
    let task_code :  String
    let channel_id : String
    let video_id : String
    let end_date: String
    let post_url: String
    let points: String
    var taskComplete: String
    let task_media_set: [MediaSet]
    let instruction_description : String
    let instruction_video_url : String
    var total_survay_form : String
    var total_survay_form_complete : String
    let latitude  : String
    let longitude : String
    let radius  : String
    let twitter_follow_id : String
    let twitter_follow_name : String
    let facebook_follow_name : String

    
    
    init(from json: JSON) {
        self.text = json["text"].stringValue
        self.district = json["district"].stringValue
        let action = json["action"].stringValue
        self.action = TaskAction(rawValue: action)
        self.state = json["state"].stringValue
        self.start_date = json["start_date"].stringValue
        self.registeration_no = json["registeration_no"].stringValue
        self.pc = json["pc"].stringValue
        self.created_date = json["created_date"].stringValue
        self.task_title = json["task_title"].stringValue
        self.task_description = json["task_description"].stringValue
        self.college = json["college"].stringValue
        let task_type = json["task_type"].stringValue
        self.task_type = TaskType(rawValue: task_type)
        self.ac = json["ac"].stringValue
        self.task_status = json["task_status"].stringValue
        self.post_id = json["post_id"].stringValue
        self.task_id = json["task_id"].stringValue
        self.end_date = json["end_date"].stringValue
        self.post_url = json["post_url"].stringValue
        self.points = json["points"].stringValue
        self.taskComplete = json["task_completed"].stringValue
        self.task_media_set = json["task_media_set"].arrayValue.map({ (media) -> MediaSet in
            return MediaSet(json: media)
        })
        self.gender = json["gender"].stringValue
        self.instruction_description = json["instruction_description"].stringValue
        self.instruction_video_url = json["instruction_video_url"].stringValue
        total_survay_form_complete = json["total_survay_form_complete"].stringValue
        total_survay_form = json["total_survay_form"].stringValue
        latitude = json["latitude"].stringValue
        longitude = json["longitude"].stringValue
        radius = json["radius"].stringValue
        self.channel_id = json["channel_id"].stringValue
        self.video_id = json["video_id"].stringValue
        self.twitter_follow_id = json["twitter_follow_id"].stringValue
        self.twitter_follow_name = json["twitter_follow_name"].stringValue
        self.task_code = json["task_code"].stringValue
        self.facebook_follow_name = json["facebook_follow_name"].stringValue




    }
    
    static func getTabTaskModel(from json: JSON)->[TaskTabModel]{
        
        return json.arrayValue.map({TaskTabModel(from: $0)})
    }
}


