//
//  PollModel.swift
//  Stalinani
//
//  Created by HariTeju on 30/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

public enum PollCategory: String{
    
    case none
    case linear
    case radio
    case textarea //news
    case selectlist

    public init(rawValue: String){
        
        switch rawValue {
        case "1":
            self = .linear
        case "2":
            self = .radio
        case "3":
            self = .textarea
        case "4":
            self = .selectlist
        default:
            self = .none
        }
    }
}

class MainRootClass : NSObject{
    
    var CODE : Int?
    var MESSAGE : String?
    var NEXT : String?
    var RESULT : [MainRESULT]?
    var TOTAL : String?
    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.MESSAGE = json["MESSAGE"].stringValue
        self.CODE = json["CODE"].intValue
        self.NEXT = json["NEXT"].stringValue
        self.TOTAL = json["TOTAL"].stringValue
        if let results = json["RESULT"].array { self.RESULT = results.map { MainRESULT(json: $0) } }
    }
}

class MainRESULT : NSObject{
    
    var createdOn : String?
    var isvoted : String?
    var pollHeading : String?
    var pollId : String?
    var pollOption : [PollOptionResult]?
    var pollQuestion : String?
    var pollType : String?
    var type : String?
    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.createdOn = json["created_on"].stringValue
        self.isvoted = json["isvoted"].stringValue
        self.pollHeading = json["poll_heading"].stringValue
        self.pollId = json["poll_id"].stringValue
        self.pollQuestion = json["poll_question"].stringValue
        self.pollType = json["poll_type"].stringValue
        self.type = json["type"].stringValue
        if let polloptionresults = json["poll_option"].array { self.pollOption = polloptionresults.map { PollOptionResult(json: $0) } }
        
    }
}

class PollOptionResult : NSObject{
    
    var optionname : String?
    var vote_count : String?
    var option_id : String?
    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.optionname = json["option_name"].stringValue
        self.vote_count = json["vote_count"].stringValue
        self.option_id = json["option_id"].stringValue
    }
}

