//
//  NewsModel.swift
//
//  Created by Appinventiv on 03/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public enum NewsCategory: String{
    
    case none
    case gallery
    case article
    case post //news
    case notification
    case banner
    case form
    public init(rawValue: String){
        
        switch rawValue {
        case "1":
            self = .gallery
        case "2":
            self = .article
        case "3":
            self = .post
        case "4":
            self = .notification
        case "5":
            self = .banner
        case "6":
            self = .form
        default:
            self = .none
        }
    }
}
public class NewsModel {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let message = "MESSAGE"
        static let total = "TOTAL"
        static let next = "NEXT"
        static let news = "RESULT"
        static let code = "CODE"
    }
    
    // MARK: Properties
    public var message: String?
    public var total: String?
    public var next: Int?
    public var news: [News]?
    public var code: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        message = json[SerializationKeys.message].string
        total = json[SerializationKeys.total].string
        next = json[SerializationKeys.next].int
        if let items = json[SerializationKeys.news].array { news = items.map { News(json: $0) } }
        code = json[SerializationKeys.code].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = message { dictionary[SerializationKeys.message] = value }
        if let value = total { dictionary[SerializationKeys.total] = value }
        if let value = next { dictionary[SerializationKeys.next] = value }
        if let value = news { dictionary[SerializationKeys.news] = value.map { $0.dictionaryRepresentation() } }
        if let value = code { dictionary[SerializationKeys.code] = value }
        return dictionary
    }
    
}

public class News {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let createdDate = "created_date"
        static let newsImage = "news_image"
        static let newsTitle = "news_title"
        static let newsDescription = "news_description"
        static let newsId = "news_id"
        static let mediaType = "media_type"
        static let mediaSet = "content_media_set"
        static let newsCategory = "news_category"
        
        static let banner_height = "banner_height"
    }
    
    // MARK: Properties
    public var createdDate: String?
    public var newsImage: String?
    public var newsTitle: String?
    public var newsDescription: String?
    public var newsId: String?
    public var mediaType: String?
    public var mediaSet: [MediaSet]?
    public var newsCategory: NewsCategory = .none
    public var banner_height : String?
    public var url : String?
    public var is_completed : String?
    public var isSelected : Bool = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        createdDate = json[SerializationKeys.createdDate].string
        newsImage = json[SerializationKeys.newsImage].string
        newsTitle = json[SerializationKeys.newsTitle].string
        newsDescription = json[SerializationKeys.newsDescription].string
        newsId = json[SerializationKeys.newsId].string
        mediaType = json[SerializationKeys.mediaType].string
        banner_height = json["banner_height"].string
        mediaSet =  json[SerializationKeys.mediaSet].array?.map({ MediaSet(json: $0)})
        url = json["url"].string
        
        let newsCategory = json[SerializationKeys.newsCategory].stringValue
        self.newsCategory = NewsCategory(rawValue: newsCategory)
        is_completed = json["is_completed"].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = createdDate { dictionary[SerializationKeys.createdDate] = value }
        if let value = newsImage { dictionary[SerializationKeys.newsImage] = value }
        if let value = newsTitle { dictionary[SerializationKeys.newsTitle] = value }
        if let value = newsDescription { dictionary[SerializationKeys.newsDescription] = value }
        if let value = newsId { dictionary[SerializationKeys.newsId] = value }
        if let value = mediaType { dictionary[SerializationKeys.mediaType] = value }
        dictionary[SerializationKeys.newsCategory] = newsCategory.rawValue
        return dictionary
    }
    
}

public class MediaSet {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let mediaThumb = "media_thumb"
        static let mediaType = "media_type"
        static let mediaUrl = "media_url"
    }
    // MARK: Properties
    public var mediaThumb: String?
    public var mediaType: String?
    public var mediaUrl: String?
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    public required init(json: JSON) {
        mediaThumb = json[SerializationKeys.mediaThumb].string
        mediaType = json[SerializationKeys.mediaType].string
        mediaUrl = json[SerializationKeys.mediaUrl].string
    }
}
