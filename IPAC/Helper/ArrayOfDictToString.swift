

import Foundation


class ArrayOfDictToString {
    
    class func jSONOfArrayOfDict(_ value:Array<Any>) -> String
    {
        do
        {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
            return String(data: jsonData as Data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as String
        }
        catch
        {
            print(error.localizedDescription)
        }
        return ""
    }
    
    class func toJson(_ value:Array<Any>) -> String {
        do {
            
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
            var string = String(data: jsonData as Data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            string = string.replacingOccurrences(of: "\n", with: "") as String
            string = string.replacingOccurrences(of: "\\", with: "") as String

            return string
        }
        catch let error as NSError{
            return ""
        }
    }
    
    
    
    class func toJsonDict(_ value:[String : Any]) -> String {
        do {
            
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
            var string = String(data: jsonData as Data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            string = string.replacingOccurrences(of: "\n", with: "") as String
            string = string.replacingOccurrences(of: "\\", with: "") as String
            
            return string
        }
        catch let error as NSError{
            return ""
        }
    }
    
}

class DictToString{
    func convertToJson(dict : [String : Any]) -> String {
        do {
            let data = self
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
            var string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) ?? ""
            string = string.replacingOccurrences(of: "\n", with: "") as NSString
            print(string)
            string = string.replacingOccurrences(of: "\\", with: "") as NSString
            print(string)
            //            string = string.replacingOccurrences(of: "\"", with: "") as NSString
            string = string.replacingOccurrences(of: " ", with: "") as NSString
            print(string)
            return string as String
        }
        catch let error as NSError{
            print(error.description)
            return ""
        }
    }
}


//Array to string
//String(describing : arrayName)
