//
//  ApiKeys.swift
//  IPAC
//
//  Created by macOS on 10/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

struct ApiKeys {
    
    static let userId           = "userId"
    static let password         = "password"
    static let code             = "CODE"
    static let result           = "RESULT"
    static let message          = "MESSAGE"
    static let total            = "TOTAL"
    static let next             = "NEXT"
    static let count            = "count"
    static let loginType = "login_type"
    static let phoneNo = "phone_no"
    static let otp = "otp"
    static let deviceToken = "device_token"
    static let start_date = "start_date"
    static let end_date = "end_date"
    static let status      = "status"
    static let accessToken = "Accesstoken"
    static let helpLineNo = "HELPLINENO"
    static let categoryId = "category_id"
    static let contactContent = "contact_content"
    static let flag           = "flag"
    static let fullName     = "full_name"
    static let emailId      = "email_id"
    static let phoneNumber      = "phone_number"
    static let whatsappNo   = "whatsup_number"
    static let gender       = "gender"
    static let dob          = "dob"
    static let displayDate  = "displayDate"
    static let college      = "college"
    static let areYouClgStudent = "is_user_college_student"
    static let state        = "state"
    static let district     = "district"
    static let districtName = "districtName"
    static let device_id    = "device_id"
    static let stateCode    = "state_code"
    static let districtCode = "district_code"
    static let collegeName  = "college_name"
    static let referalCode  = "referal_code"
    static let stateName    = "stateName"
    static let isNewCollege = "is_new_college"
    static let acceptTermNdCondition = "acceptTermNdCondition"
    static let registeration_no = "registeration_no"
    static let phone_Number = "phone_number"
    static let profileImage = "profileImage"
    static let userImage   = "user_image"
    static let taskId       = "task_id"
    static let proofType   = "proof_type"
    static let proofImage     = "proof_image"
    static let idNumber            = "id_number"
    static let user_id          = "user_id"
    static let facebookId           = "facebookId"
    static let twitterId            = "twitterId"
    static let fbUsername           = "fbUsername"
    static let twitterUsername      = "twitterUsername"
    static let isProofUploaded      = "isProofUploaded"
    static let platform             = "platform"
    static let media = "media"
    
//sbm
    static let deactive_date = "deactive_date"
}
