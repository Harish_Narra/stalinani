//
//  StringExtension.swift
//  IPAC
//
//  Created by Admin on 11/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Photos

extension String{
    func MD5(_ string: String) -> String? {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        if let d = string.data(using: String.Encoding.utf8) {
            _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
                CC_MD5(body, CC_LONG(d.count), &digest)
            }
        }
        
        return (0..<length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }

}



extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension String{
    
    func checkIfValid(_ validityExression : ValidityExression) -> Bool {
        
        let regEx = validityExression.rawValue
        
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        
        return test.evaluate(with: self)
    }
    
    func checkIfInvalid(_ validityExression : ValidityExression) -> Bool {
        
        return !self.checkIfValid(validityExression)
    }
    
    ///Returns a localized string
    var localized:String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from:fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor, font: UIFont) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        self.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: range)
        self.addAttribute(NSAttributedStringKey.font, value: font, range: range )
    }
}

enum ValidityExression : String {
    
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,}"
    case mobileNumber = "^[0-9]{10}$"
    case password = "^[a-zA-Z0-9!@#$%&*]{3,}"
    case name = "^[a-zA-Z]{2,15}"
  
}

extension String{
    func downloadVideoLinkAndCreateAsset(_ videoLink: String) {
        CommonFunction.showLoader()
        guard let videoURL = URL(string: videoLink) else {
            CommonFunction.hideLoader()
            return
        }
        guard let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            CommonFunction.hideLoader()
            return
        }
        if FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path) {
            do{
                try FileManager.default.removeItem(atPath: documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path)
            }catch{
                print_debug(/error.localizedDescription)
                CommonFunction.hideLoader()

            }
        }
        URLSession.shared.downloadTask(with: videoURL) { (location, response, error) -> Void in
            guard let location = location else {
                CommonFunction.hideLoader()
                return
            }
            let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? videoURL.lastPathComponent)
            do {
                try FileManager.default.moveItem(at: location, to: destinationURL)
                PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                    if authorizationStatus == .authorized {
                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: destinationURL)}) { completed, error in
                                if completed {
                                    CommonFunction.hideLoader()
                                    CommonFunction.showToast(msg: AlertMessage.fileSavedSuccessfully.localized)
                                } else {
                                    CommonFunction.hideLoader()
                                    print_debug(/error?.localizedDescription)
                                }
                        }
                    }else{
                        CommonFunction.hideLoader()
                        CommonFunction.showToast(msg: AlertMessage.allowPermission.localized)
                    }
                })
                
            } catch {
                CommonFunction.hideLoader()
                print(error.localizedDescription)
            }
            
            }.resume()

    }
}
