
import UIKit
import Photos
import EZSwiftExtensions


class CameraGalleryPickerBlock: NSObject , UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    typealias onPicked = (UIImage, String) -> ()
    typealias onCanceled = () -> ()
    
    var pickedListner : onPicked?
    var canceledListner : onCanceled?
    
    static let shared = CameraGalleryPickerBlock()
    
    override init(){
        super.init()
    }
    
    deinit{
        
    }
    func pickerImage(button : [String] ,pickedListner : @escaping onPicked , canceledListner : @escaping onCanceled){
        
        Utility.functions.showActionSheetWithStringButtons(buttons: button, success: {[unowned self] (str) in
            
            if str == "Camera"{
                
                Utility.functions.isCameraPermission(actionOkButton: { [weak self] (isOk) in
                    if !isOk{
                        ez.runThisInMainThread {
                            self?.showAlert("Allow Permission", "Please allow permission from settings")
                        }
                        
                    }else {
                        self?.pickedListner = pickedListner
                        self?.canceledListner = canceledListner
                        self?.showCameraOrGallery(type: str)
                    }
                    
                })
            }else if str == "Cancel"{
                canceledListner()
            }
                
            else {
                
                Utility.functions.accessToPhotos(actionOkButton: { (isOk) in
                    
                    if !isOk{
                        ez.runThisInMainThread {
                            self.showAlert("Allow Permission", "Please allow permission from settings")
                        }
                    }else {
                        self.pickedListner = pickedListner
                        self.canceledListner = canceledListner
                        self.showCameraOrGallery(type: str)
                    }
                })
            }
        })
    }
    
    func showAlert(_ title : String? , _ message : String?){
        
        AlertsClass.shared.showAlertController(withTitle: /title, message: /message, buttonTitles: ["Settings","Cancel"]) { [weak self] (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                self?.openSettings()
            default:
                return
            }
        }
    }
    
    func showCameraOrGallery(type : String){
        let picker : UIImagePickerController = UIImagePickerController()
        picker.sourceType = /type == "Camera" ? UIImagePickerControllerSourceType.camera : UIImagePickerControllerSourceType.photoLibrary
        if(UIImagePickerController.isSourceTypeAvailable(/type == "Camera" ? UIImagePickerControllerSourceType.camera : UIImagePickerControllerSourceType.photoLibrary)){
            picker.delegate = self
            picker.allowsEditing = true
            
            ez.runThisInMainThread {
                ez.topMostVC?.present(picker, animated: true, completion: nil)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true , completion: nil)
        
        if let listener = canceledListner{
            listener()
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true , completion: nil)
        
        
        
        var fileName : String?
        if let image : UIImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                let assets = result.firstObject
                if let filename =  assets?.value(forKey: "filename") {
                    fileName = filename as? String
                }
            }
            
            if let listener = pickedListner{
                listener(image , /fileName)
            }
        }
    }
    
    
    func openSettings(){
        
        guard let settingsUrl = URL(string:UIApplicationOpenSettingsURLString) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
