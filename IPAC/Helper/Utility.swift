
import UIKit
import Photos
import CoreLocation
import MapKit
import EZSwiftExtensions
import NVActivityIndicatorView


typealias success = (_ coordinates: CLLocationCoordinate2D, _ fullAddress: String?, _ name : String?, _ city : String?,_ state : String?, _ subLocality: String? ) -> ()

class Utility: NSObject {
    static let functions = Utility()
    let geoCoder = CLGeocoder()
    let formatter = DateFormatter()
    
    override init() {
        super.init()
    }
    
    @available(iOS 10.0, *)
    static func shared() -> AppDelegate {
        return (UIApplication.shared.delegate as! AppDelegate)
    }

    func loader(_ new : Bool = false)  {
        ez.runThisInMainThread {
            Utility.shared().window?.rootViewController?.startAnimating(nil, message: nil, messageFont: nil, type: NVActivityIndicatorType.circleStrokeSpin, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)

        }
    }
    
    func removeLoader()  {
        ez.runThisInMainThread {
            Utility.shared().window?.rootViewController?.stopAnimating()

        }
    }
    
    func showActionSheetWithStringButtons(  buttons : [String] , success : @escaping (String) -> ()) {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        for button in buttons{
            let action = UIAlertAction(title: button , style: .default, handler: { (action) -> Void in
                success(button)
            })
            controller.addAction(action)
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (button) -> Void in
        }
        controller.addAction(cancel)
        controller.view.tintColor = UIColor.floatingTitleColor
        
        ez.topMostVC?.present(controller, animated: true) { () -> Void in
        }
    }
    

    func isCameraPermission(actionOkButton: ((_ isOk: Bool) -> Void)? = nil){
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
        case .authorized: actionOkButton!(true)
        case .restricted: actionOkButton!(false)
        case .denied: actionOkButton!(false)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    actionOkButton!(true)
                }else {
                    actionOkButton!(false)
                }
            })
        }
    }
    
    func calculateAddress(lat : CLLocationDegrees , long : CLLocationDegrees , responseBlock : @escaping success){
        geoCoder.reverseGeocodeLocation( CLLocation(latitude: lat, longitude: long), completionHandler: { [weak self] (placemarks, error) -> Void in
            let placeMark = placemarks?.first
            guard let address = placeMark?.addressDictionary?["FormattedAddressLines"] as? [String] else {return}
            let fullAddress = address.joined(separator: ", ")
            let name = placeMark?.addressDictionary?["Name"] as? String
            let city = placeMark?.addressDictionary?["City"] as? String
            let state = placeMark?.addressDictionary?["State"] as? String
            let subLocality = placeMark?.addressDictionary?["SubLocality"] as? String
            responseBlock(CLLocationCoordinate2D(latitude: lat, longitude: long), fullAddress,name,city,state,subLocality)
        })
        
    }
    
    func openMap(lat: String, lng: String, name: String?) {
        let destLat: Float = Float(lat) ?? 0.0
        let destLong: Float = Float(lng) ?? 0.0
        let destLocation = CLLocationCoordinate2DMake(CLLocationDegrees(destLat), CLLocationDegrees(destLong))
        let placeMark = MKPlacemark(coordinate: destLocation, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placeMark)
        mapItem.name = /name
        mapItem.openInMaps(launchOptions: nil)
    }
    

    

    
    func accessToPhotos(actionOkButton: ((_ isOk: Bool) -> Void)? = nil){
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized: actionOkButton!(true)
        case .restricted: actionOkButton!(false)
        case .denied: actionOkButton!(false)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                    actionOkButton!(true)
                }else {
                    actionOkButton!(false)
                }
            })
        }
    }
}
    protocol StringType { var get: String { get } }
    
    extension String: StringType { var get: String { return self } }
    
    extension Optional where Wrapped: StringType {
        func unwrap() -> String {
            return self?.get ?? ""
        }
    }
    
    
    prefix operator /
    prefix func /(value : String?) -> String {
        return value.unwrap()
}

extension UIViewController : NVActivityIndicatorViewable{
    
}
