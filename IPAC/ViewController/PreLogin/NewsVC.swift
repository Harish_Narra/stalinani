//
//  NewsVC.swift
//  IPAC
//
//  Created by Appinventiv on 12/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
class NewsVC: UIViewController {
    
    // MARK:- VARIABLES
    //====================
    var date = "26 June, 3:32 PM"
    var newsHeadline = "Jagan promises all help to Nagaram blast victims"
    var newsBody = "YSR Congress Party president Y.S. Jagan Mohan Reddy on Sunday assured the victims of the Nagaram pipeline explosion that their problems would be solved once the party comes to power in the 2019 elections."
    
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var navigationTitleLabel: UILabel!
    
    @IBOutlet weak var newsTableView: UITableView!
    
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.dropShadow(color: UIColor.init(red: 0, green: 0, blue: 0, alpha: 1), opacity: 0.09, offSet: .zero, radius: 7.5, scale: true)
    }
    
    // MARK:- IBACTIONS
    //====================
    
    @IBAction func profileBtnTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func notificationsBtnTapped(_ sender: UIButton) {
        
    }
    
}



// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension NewsVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openNewsDetail()
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCellid", for: indexPath) as? NewsCell else{
            fatalError("News Cell not found")
        }
        if indexPath.row == 2{
            cell.newsHeadingLabel.text = newsHeadline+newsHeadline+newsHeadline
            cell.newsBodyLabel.text = newsBody+newsBody+newsHeadline
        }
        else{
            cell.newsHeadingLabel.text = newsHeadline
            cell.newsBodyLabel.text = newsBody
        }
        cell.dateTimeLabel.text = date
        cell.readMoreBtn.addTarget(self, action: #selector(openNewsDetail), for: .touchUpInside)
        return cell
    
    }
    
}

// MARK:- FUNCTIONS
//====================
extension NewsVC {
    
    /// Invite Page Content Initial Setup
    private func initialSetup() {
        navigationTitleLabel.text = AppStrings.News.rawValue.localized
        newsTableView.delegate = self
        newsTableView.dataSource = self
        newsTableView.register(UINib.init(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCellid")
    }
    
    @objc private func openNewsDetail()
    {
        let newsDetailVC = NewsDetailVC.instantiate(fromAppStoryboard: .Home)
        navigationController?.pushViewController(newsDetailVC, animated: true)
    }
}


