//
//  YoutubeCommentPopupVC.swift
//  IPAC
//
//  Created by Appinventiv on 16/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

protocol YoutubeCommentDelegate : class {
    func postComment()
}

class YoutubeCommentPopupVC: BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var lblDesc: UILabel!
    
    
    //MARK::- PROPERTIES
    var task : TaskTabModel?
    weak var delegate : YoutubeCommentDelegate?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionOK(_ sender: UIButton) {
        delegate?.postComment()
        self.dismiss(animated: false, completion: nil)

    }
    
    @IBAction func btnActionDismiss(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

//MARK::- FUNCTION
extension YoutubeCommentPopupVC{
    func onViewDidLoad(){
        lblDesc.text = /task?.task_description
    }
}
