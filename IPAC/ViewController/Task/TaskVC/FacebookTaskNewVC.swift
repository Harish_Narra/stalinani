//
//  FacebookTaskNewVC.swift
//  
//
//  Created by HariTeju on 18/06/20.
//

import UIKit
import WebKit

protocol FacebookTaskDelegate : class {
    func refresh()
}

class FacebookTaskNewVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var backBtn: UIButton!
    
    //var task : TaskTabModel?
    private var jsHandler = ""
      private var mpAjaxHandler = "mpAjaxHandler"
      
      var isNeedToCheckContent = false
      var task : TaskTabModel?
      weak var delegate : FacebookTaskDelegate?
      var currentUrl = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.onViewDidLoad()

//        self.webView.navigationDelegate = self
//
//        let urlToLoad = "https://www.google.com/"
//
//        if let url =  URL(string: urlToLoad){
//            let request = URLRequest(url:url)
//            self.webView.load(request)
//        }
        // Do any additional setup after loading the view.
    }
    @IBAction func backClikced(_ sender: Any) {
        if !isPost(str : /self.task?.post_url){
                   self.popVC()
            print(/(self.task?.action).map { $0.rawValue })
            print(/self.task?.post_url)
            print(/self.task?.taskComplete)
            
            if self.task?.taskComplete == "0" {
                isNeedToCheckContent = true
                              updateTask()
            } else {
                CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                return
            }


           
        } else if isShare(str : /self.task?.post_url){
                   self.popVC()
            print(/(self.task?.action).map { $0.rawValue })
            print(/self.task?.post_url)
            print(/self.task?.taskComplete)
            
            if self.task?.taskComplete == "0" {
                isNeedToCheckContent = true
                              updateTask()
            } else {
                CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                return
            }


           
        } else {
              isNeedToCheckContent = true
            
              self.popVC()
              CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)


            
//               if let url =  URL(string: /self.task?.post_url){
//                   let request = URLRequest(url:url)
//                   self.webView.load(request)
//               }else{
//                   self.popVC()
//                   CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
//               }
        }
    }
    func onViewDidLoad(){
        self.webView.navigationDelegate = self
        
        if self.task?.post_url != "" {

        //webView.delegate = self
            if let url =  URL(string: /self.task?.post_url){
                let request = URLRequest(url:url)
                self.webView.load(request)
            }
            if !self.isPost(str: /self.task?.post_url){
                if let anExtension = Bundle.main.url(forResource: "EventHandler", withExtension: "js") {
                    do {
                        jsHandler = try String(contentsOf: anExtension, encoding: String.Encoding.utf8)
                    }
                    catch let error {
                        print("Error: \(error)")
                    }
                }
            }
        } else {
            let addpage = self.task?.facebook_follow_name
            let pageURL = "https://www.facebook.com/\(addpage ?? "")"
            if let url =  URL(string: pageURL){
                            let request = URLRequest(url:url)
                            self.webView.load(request)
                        }
                        if !self.isPostFollow(urls: pageURL, id:/CommonFunction.getUser()?.facebookId){
                            print(pageURL,/CommonFunction.getUser()?.facebookId)
                            if let anExtension = Bundle.main.url(forResource: "EventHandler", withExtension: "js") {
                                do {
                                    jsHandler = try String(contentsOf: anExtension, encoding: String.Encoding.utf8)
                                }
                                catch let error {
                                    print("Error: \(error)")
                                }
                            }
                        }
                        if !CommonFunction.isFbPopupHidden(){
                            let vc = TwitterFollowStepPopupVC.instantiate(fromAppStoryboard: .Form)
                            vc.isFacebook = true
                            self.presentVC(vc)
                        }
        }
        if self.isLike(str: /self.task?.post_url) {
            if let anExtension = Bundle.main.url(forResource: "EventHandler", withExtension: "js") {
                do {
                    jsHandler = try String(contentsOf: anExtension, encoding: String.Encoding.utf8)
                }
                catch let error {
                    print("Error: \(error)")
                }
            }
            
        }
        if self.isShare(str: /self.task?.post_url) {
            if let anExtension = Bundle.main.url(forResource: "EventHandler", withExtension: "js") {
                do {
                    jsHandler = try String(contentsOf: anExtension, encoding: String.Encoding.utf8)
                }
                catch let error {
                    print("Error: \(error)")
                }
            }
            
        }
            if !CommonFunction.isFbPopupHidden(){
                let vc = TwitterFollowStepPopupVC.instantiate(fromAppStoryboard: .Form)
                vc.isFacebook = true
                self.presentVC(vc)
            }
        }
        func isPost(str : String) -> Bool{
            return (str.contains("/posts") || str.contains("/videos") || str.contains("/photos"))
        }
    func isLike(str : String) -> Bool{
        return (str.contains("/posts") || str.contains("/videos") || str.contains("/photos")  || str.contains("/like")  || str.contains("/likes")  || str.contains("/follow_mutator")  || str.contains("/subscription") || str.contains("/subscriptions"))
    }
    func isShare(str : String) -> Bool{
           return (str.contains("/share") || str.contains("/post") || str.contains("/posts") || str.contains("/videos") || str.contains("/photos")  || str.contains("/like")  || str.contains("/likes")  || str.contains("/follow_mutator")  || str.contains("/subscription") || str.contains("/subscriptions"))
       }

//        if self.task?.post_url != "" {
//            if let url =  URL(string: /self.task?.post_url){
//                let request = URLRequest(url:url)
//                self.webView.load(request)
//            }
//            if !self.isPost(str: /self.task?.post_url){
//                if let anExtension = Bundle.main.url(forResource: "EventHandler", withExtension: "js") {
//                    do {
//                        jsHandler = try String(contentsOf: anExtension, encoding: String.Encoding.utf8)
//                    }
//                    catch let error {
//                        print("Error: \(error)")
//                    }
//                }
//            }
//            if !CommonFunction.isFbPopupHidden(){
//                let vc = TwitterFollowStepPopupVC.instantiate(fromAppStoryboard: .Form)
//                vc.isFacebook = true
//                self.presentVC(vc)
//            }
//        } else {
//            let pageURL = "https://www.google.co.in/"
//            if let url =  URL(string: pageURL){
//                let request = URLRequest(url:url)
//                self.webView.load(request)
//            }
//            if !self.isPostFollow(urls: pageURL, id:/CommonFunction.getUser()?.facebookId){
//                print(pageURL,/CommonFunction.getUser()?.facebookId)
//                if let anExtension = Bundle.main.url(forResource: "EventHandler", withExtension: "js") {
//                    do {
//                        jsHandler = try String(contentsOf: anExtension, encoding: String.Encoding.utf8)
//                    }
//                    catch let error {
//                        print("Error: \(error)")
//                    }
//                }
//            }
//            if !CommonFunction.isFbPopupHidden(){
//                let vc = TwitterFollowStepPopupVC.instantiate(fromAppStoryboard: .Form)
//                vc.isFacebook = true
//                self.presentVC(vc)
//            }
//        }
 //   }
//    func isPost(str : String) -> Bool{
//        return (str.contains("/posts") || str.contains("/videos") || str.contains("/photos"))
//
//    }
    func isPostFollow(urls : String, id: String) -> Bool{

      return (urls.contains("subscriptions") || urls.contains("subscription") && urls.contains("add") && urls.contains(id)
        || (urls.contains("follow_mutator") && urls.contains("status=true") && urls.contains(id)))
    }

}
extension FacebookTaskNewVC: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        CommonFunction.showLoader()

        print("Started to load")
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"

                        webView.evaluateJavaScript(jscript)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        CommonFunction.hideLoader()
               
              
                   currentUrl = /webView.url?.absoluteString
                     if isNeedToCheckContent{

                       evaluate(script: "document.documentElement.outerHTML") { (resul, error) in
                           print(resul ?? "")
                       }


                     }
                  print("two")
    }
    func evaluateJavaScript(_ javaScriptString: String,
                            completionHandler: ((_ res: String?, Error?) -> Void)? = nil) {

    }
    func evaluate(script: String, completion: @escaping (Any?, Error?) -> Void) {
        var finished = false

        evaluateJavaScript(script, completionHandler: { (result, error) in
            if error == nil {
                if result != nil {
                    completion(result, nil)
                    if self.isPost(str: /self.task?.post_url) && (result?.contains("/ufi/reaction"))! ||  !self.isPost(str: /self.task?.post_url) && (result?.contains("add=true"))!{
                        self.updateTask()
                                       }else{
                                           CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                                           self.popVC()
                                       }
                }
            } else {
                completion(nil, error)
            }
            finished = true
        })

        while !finished {
            RunLoop.current.run(mode: RunLoop.Mode(rawValue: "NSDefaultRunLoopMode"), before: NSDate.distantFuture)
        }
    }
  
     func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
               CommonFunction.hideLoader()
               CommonFunction.showToast(msg: error.localizedDescription)
               print("three")
               
           }
           func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
               CommonFunction.hideLoader()
               CommonFunction.showToast(msg: error.localizedDescription)
               print("four")
               
           }
           func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
                let str = navigationAction.request.url?.absoluteString

                let taskComplete = str?.contains("add=true")
            if ((navigationAction.request.url?.scheme?.lowercased().isEqual(mpAjaxHandler.lowercased()) ?? false) && taskComplete!)  {
                    let arrOne = self.task?.post_url.split("facebook.com/")
                    let arrTwo = self.currentUrl.split("facebook.com/")
                    let userNameFromBackend = arrOne?.last?.replacingOccurrences(of: "/", with: "")
                    let userNameInWeb = arrTwo.last?.replacingOccurrences(of: "/", with: "")
                    if userNameFromBackend == userNameInWeb{
                        self.updateTask()
                    }
                               decisionHandler(.cancel)
                }
                decisionHandler(.allow)
           }
}
//{
//    print_debug(/request.url?.absoluteString)
//    let str = /request.url?.absoluteString
//    let taskComplete = str.contains("add=true")
//    if ((request.url?.scheme?.lowercased().isEqual(mpAjaxHandler.lowercased()) ?? false) && taskComplete)  {
//        let arrOne = self.task?.post_url.split("facebook.com/")
//        let arrTwo = self.currentUrl.split("facebook.com/")
//        let userNameFromBackend = arrOne?.last?.replacingOccurrences(of: "/", with: "")
//        let userNameInWeb = arrTwo.last?.replacingOccurrences(of: "/", with: "")
//        if userNameFromBackend == userNameInWeb{
//            self.updateTask()
//        }
//        return false
//    }
//    return true
//}
extension FacebookTaskNewVC{
    private func updateTask(){
        let params = [ApiKeys.taskId: /self.task?.task_id]
        WebServices.updateTask(params: params, success: { [weak self](result) in
            print_debug(result)
            self?.delegate?.refresh()
            self?.navigationController?.popViewController(animated: false)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            self.popVC()
        }
    }
}
