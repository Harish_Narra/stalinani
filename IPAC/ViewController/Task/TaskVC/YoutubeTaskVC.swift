//
//  YoutubeTaskVC.swift
//  IPAC
//
//  Created by Appinventiv on 15/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import WebKit
import GoogleSignIn

protocol YoutubeTaskDelegate : class {
    func refresh()
}

class YoutubeTaskVC: BaseViewController, WKNavigationDelegate {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnType: GradientButton!
    @IBOutlet weak var webView: WKWebView!
    
    //MARK::- PROPERTIES
    var task : TaskTabModel?
    weak var delegate : YoutubeTaskDelegate?
    var urlToLoad = ""
    
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonFunction.showLoader()

        onViewDidLoad()
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        CommonFunction.showLoader()
        print("one")
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
         
         webView.evaluateJavaScript(jscript)
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CommonFunction.hideLoader()
        print("two")
       
        //CommonFunction.hideLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CommonFunction.hideLoader()
        CommonFunction.showToast(msg: error.localizedDescription)
        print("three")
        
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        CommonFunction.hideLoader()
        CommonFunction.showToast(msg: error.localizedDescription)
        print("four")
        
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
       
        decisionHandler(.allow)
    }
    
    //MARK::- BUTTON ACTIONS
    
    @IBAction func btnAction(_ sender: UIButton) {
        
        switch (self.task?.action ?? .share){
        case .comment:
            let vc = YoutubeCommentPopupVC.instantiate(fromAppStoryboard: .Form)
            vc.delegate = self
            vc.task = task
            self.presentVC(vc)
            break
        case .share:
            openShareSheet()
            break
        case .subscribe:
            googleLoginSubscribe()
            break
        case .like:
            googleLoginLike()
            break
        default:
            break
        }
        
    }
    
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.popVC()
        CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
        
    }
    
}



//MARK::- COMMENT POPUP DELEGATE
extension YoutubeTaskVC : YoutubeCommentDelegate{
    func postComment() {
        googleLoginComment()
    }
}

//MARK::- FUNCTIONS
extension YoutubeTaskVC{
    func onViewDidLoad(){
        self.webView.navigationDelegate = self
        btnType.setTitle(/task?.action.rawValue.uppercased(),for : .normal)
        if self.task?.action == .subscribe{
            urlToLoad = "https://www.youtube.com/channel/\(/self.task?.channel_id)"
        }else{
            urlToLoad = "https://www.youtube.com/watch?v=\(/self.task?.video_id)"
        }
        if let url =  URL(string: urlToLoad){
            let request = URLRequest(url:url)
            self.webView.load(request)
        }
        if !CommonFunction.isYoutubePopupHidden(){
            let vc = YoutubeStepPopupVC.instantiate(fromAppStoryboard: .Form)
            self.presentVC(vc)
        }
    }
    
    func openShareSheet(){
        
        let someText:String =  /self.task?.task_title + " " + self.urlToLoad
        let activityViewController = UIActivityViewController(activityItems : [someText], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop]
        activityViewController.completionWithItemsHandler = { activity, success, items, error in
            if !success{
                CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                return
            }else{
                self.updateTask()

            }

        }
        self.present(activityViewController, animated: true) {
        }
    }
    
    func googleLoginSubscribe(){
        GIDSignIn.sharedInstance()?.presentingViewController = self

        GoogleLoginController.shared.login(success: { (googleUser) in
            print_debug(googleUser)
            let  dict = ["kind" : "youtube#channel" , "channelId" : /self.task?.channel_id]
            let ParentDict  = ["resourceId" : dict]
            let superParentDict : [String : Any] = ["snippet" : ParentDict]
            WebServices.postAddSubscription(token : googleUser.idToken,params: superParentDict, success: { (json) in
                print_debug(json)
                self.updateTask()
            }, failure: { (err : Error) -> (Void) in
                print_debug(err.localizedDescription)
                CommonFunction.showToast(msg: err.localizedDescription)
            })
            
        }) { (err : Error) in
            CommonFunction.showToast(msg: err.localizedDescription)
        }
    }
    func googleLoginComment(){
        GIDSignIn.sharedInstance()?.presentingViewController = self

        GoogleLoginController.shared.login(success: { (googleUser) in
            print_debug(googleUser)
            let dictText = ["textOriginal" : /self.task?.task_description]
            let dictSnippet = ["snippet" : dictText]
            let topLevelComment : [String : Any]  = ["topLevelComment" : dictSnippet  , "videoId" : /self.task?.video_id]
            let  dict : [String : Any]  = [ "snippet" :  topLevelComment ]
            WebServices.postYoutubeComment(token: googleUser.idToken,params : dict  ,success: { (json) in
                print_debug(json)
                self.updateTask()
            }, failure: { (err : Error) -> (Void) in
                print_debug(err.localizedDescription)
                CommonFunction.showToast(msg: err.localizedDescription)
            })
        }) { (err : Error) in
            CommonFunction.showToast(msg: err.localizedDescription)
        }
    }
    func googleLoginLike(){
        GIDSignIn.sharedInstance()?.presentingViewController = self

        GoogleLoginController.shared.login(success: { (googleUser) in
            print_debug(googleUser)
            let dict = ["rating" : "like" , "id" :/self.task?.video_id ]
            WebServices.postYoutubeLike(id : /self.task?.video_id   , token: googleUser.idToken,params : dict  ,success: { (json) in
                print_debug(json)
                self.updateTask()
            }, failure: { (err : Error) -> (Void) in
                print_debug(err.localizedDescription)
                CommonFunction.showToast(msg: err.localizedDescription)
            })
        }) { (err : Error) in
            CommonFunction.showToast(msg: err.localizedDescription)
        }
    }
}

//MARK::- API

extension YoutubeTaskVC{
    private func updateTask(){
        let params = [ApiKeys.taskId: /self.task?.task_id]
        WebServices.updateTask(params: params, success: { [weak self](result) in
            print_debug(result)
            self?.delegate?.refresh()
            self?.navigationController?.popViewController(animated: false)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}
