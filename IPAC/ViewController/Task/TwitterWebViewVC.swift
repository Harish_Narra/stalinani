//
//  TwitterWebViewVC.swift
//  IPAC
//
//  Created by macOS on 03/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TwitterKit
import SwiftyJSON
import Alamofire
import WebKit


class TwitterWebViewVC: BaseViewController, WKNavigationDelegate  {

    //MARK::- PROPERTIES
    var taskModel : TaskTabModel!
    var user: UserModel?
    weak var delegate : TwitterTaskDelegate?
    var isRetweet = false

    //MARK::- OUTLETS
    @IBOutlet weak var backBtnContainerView: UIView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var backButton: UIButton!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonFunction.showLoader()

        self.initialSetup()
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
           CommonFunction.showLoader()
           print("one")
           let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
            
            webView.evaluateJavaScript(jscript)
           
       }
       func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
           CommonFunction.hideLoader()
           print("two")
          
           //CommonFunction.hideLoader()
       }
       
       func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
           CommonFunction.hideLoader()
           CommonFunction.showToast(msg: error.localizedDescription)
           print("three")
           
       }
       func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
           CommonFunction.hideLoader()
           CommonFunction.showToast(msg: error.localizedDescription)
           print("four")
           
       }
       func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
              if let urlStr = navigationAction.request.url?.absoluteString{
                //urlStr is what you want, I guess.
                  print(urlStr)
                  
                  print_debug(urlStr)
                                 if ((/urlStr).contains("complete?")) {
                                 }
             }
             
              //        return true
              decisionHandler(.allow)
          }

    @IBAction func backBtnTapped(_ sender: UIButton) {
        CommonFunction.showLoader()
        guard let twitterData = TWTRTwitter.sharedInstance().sessionStore.session() else { return }
        let param = ["user_id": twitterData.userID , "count": "1"] as [String : Any]
        TwitterController.shared.getTweets(params: param, success: { (tweets) in
            print_debug(tweets)
            CommonFunction.hideLoader()

            if self.isRetweet{
                let splitArr = self.taskModel?.post_url.split("tweet_id=")
                print(splitArr!)
                let some = splitArr?[0]
                print("String: ",some!)
                let fileArray = some?.components(separatedBy: "/")
                let finalFileName = fileArray?.last
                print("\"id\":"  + (finalFileName ?? ""))

              //  print(/splitArr?.last)
              //  print(tweets.contains("\"id\":"  + (finalFileName ?? "")))
               
                if tweets.contains("\"id\":"  + (finalFileName ?? "")) {
                    self.updateTask()
                } else {
                    self.navigationController?.popViewController(animated: true)
                    CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                    CommonFunction.hideLoader()
                }
            }else{
//                if tweets.contains(self.taskModel.text) {
//                    self.updateTask()
//                } else {
//                    self.navigationController?.popViewController(animated: true)
//                    CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
//                }
                if tweets.contains("text") {
                    self.updateTask()
                } else {
                    self.navigationController?.popViewController(animated: true)
                    CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                    CommonFunction.hideLoader()
                }
            }
       
        }) { (error) -> (Void) in
            print_debug(error.localizedDescription)
        }
    }
    
    //MARK::- FUNCTIONS
    private func initialSetup() {
        
        setupText()
        self.user = CommonFunction.getUser()
        self.webView.navigationDelegate = self
        if isRetweet{
            if let url =  URL(string: /self.taskModel?.post_url){
                let request = URLRequest(url:url)
                self.webView.load(request)
            }
        }else{
            if let url =  URL(string: "https://mobile.twitter.com/compose/tweet/"){
                let request = URLRequest(url:url)
                self.webView.load(request)
            }
        }
    
        if !CommonFunction.isTwitterFollowPopupHidden(){
            let vc = TwitterFollowStepPopupVC.instantiate(fromAppStoryboard: .Form)
            self.presentVC(vc)
        }
    }
    
    private func setupText() {
        self.backButton.titleLabel?.font = AppFonts.ProximaNova_Semibold.withSize(12)
        self.backButton.setTitle(StringConstant.Back_To_App.localized, for: .normal)
    }
}



//MARK::- API
extension TwitterWebViewVC{
    private func updateTask(){
        let params = [ApiKeys.taskId: /self.taskModel?.task_id]
        WebServices.updateTask(params: params, success: { [weak self](result) in
            print_debug(result)
            self?.delegate?.refresh()
            self?.navigationController?.popViewController(animated: false)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
}
}


