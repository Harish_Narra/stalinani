//
//  BonusPopupTableViewCell.swift
//  IPAC
//
//  Created by Appinventiv on 25/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class BonusPopupTableViewCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
