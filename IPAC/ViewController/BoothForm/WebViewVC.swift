//
//  WebViewVC.swift
//  IPAC
//
//  Created by Appinventiv on 25/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit


//MARK::- PROTOCOL
protocol RefreshData : class{
    func refresh()
}

protocol FormCompletedDelegate : class{
    func completed()
    
}

class WebViewVC: BaseViewController, WKNavigationDelegate {
    
    //MARK::- OUTLETS
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK::- PROPERTIES
    var url = ""
    var formComplete = false
    var task : TaskTabModel?
    var news : News?
    var flag = 0 // 0 booth 1 home 2 task
    weak var delegate : RefreshData?
    weak var delegateForm : FormCompletedDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonFunction.showLoader()

        onViewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        if formComplete == true{
            delegateForm?.completed()
        }else{
            CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
        }
        popVC()
    }
    
    
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
           CommonFunction.showLoader()
              let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
            
        webView.evaluateJavaScript(jscript)
           
       }
       func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
           CommonFunction.hideLoader()
           print("two")
         
       }
       
       func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
           CommonFunction.hideLoader()
           CommonFunction.showToast(msg: error.localizedDescription)
           print("three")
           
       }
       func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
           CommonFunction.hideLoader()
           CommonFunction.showToast(msg: error.localizedDescription)
           print("four")
           
       }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
           let urlStr = navigationAction.request.url?.absoluteString
           
           
           print_debug(urlStr)
                if flag == 2{
                    if ((/urlStr).contains("campaign-thankyou")) {
                        if navigationAction.request.url?.valueOf("status") == "complete" || navigationAction.request.url?.valueOf("status") == "pending"{
                            viewBack.isHidden = false
                            self.formComplete = true
                            delegate?.refresh()
                            self.navigationController?.popViewController(animated: false)
                            CommonFunction.hideLoader()
                           decisionHandler(.cancel)
                        }
                    }
                }else{
                    if ((/urlStr).contains("campaign-thankyou")) {
                        viewBack.isHidden = false
                        formComplete = true
                    }
                }

           decisionHandler(.allow)
       }
    
}

//MARK::- FUNCTIONS
extension WebViewVC{
    func onViewDidLoad(){
        btnBack.setTitle(AlertMessage.backToApp.localized,for : .normal)
        self.webView.navigationDelegate = self
        viewBack.isHidden = true
        var urlToLoad = ""
        switch flag{
        case 1:
            lblTitle.text = AlertMessage.form.localized
            urlToLoad = self.url + "&user_id=\(/CommonFunction.getUser()?.userId)&news_id=\(/news?.newsId)"
        case 2:
            lblTitle.text = AlertMessage.form.localized
            self.url = /self.task?.post_url
            urlToLoad = self.url + "&user_id=\(/CommonFunction.getUser()?.userId)&task_id=\(/task?.task_id)"
        default:
            lblTitle.text = AlertMessage.markYourBooth.localized
            urlToLoad =  self.url + "&user_id=\(/CommonFunction.getUser()?.userId)"
        }
        
        if let url =  URL(string: urlToLoad){
            let request = URLRequest(url:url)
            self.webView.load(request)
        }
    }
}

