//
//  UploadImageForMediaTaskVC.swift
//  IPAC
//
//  Created by Appinventiv on 02/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

protocol ImageUploaded : class{
    func success(arr : [Image])
}

class UploadImageForMediaTaskVC: BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var btnSubmit: GradientButton!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var viewFooter: AnimatableView!
    
    //MARK::- PROPERTIES

    weak var delegate : ImageUploaded?
    var imageLimit = 4
    var imgSize = 0
    var imageArray = [Image]()
    var task : TaskTabModel?
    var isUploadOption = true

    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
 
    //MARK::- BUTTON ACTION
    @IBAction func btnActionUploadFooter(_ sender: UIButton) {
        if imageLimit == self.imageArray.count {
            CommonFunction.showToast(msg: AlertMessage.maxLimitReached.localized)
            return
        }
        self.isUploadOption = true
        self.tableView.reloadData()
        
    }
    @IBAction func btnActionDismiss(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
    @IBAction func btnActionSubmit(_ sender: UIButton) {
        if self.task?.action == TaskAction.setdp && self.imageArray.count != imageLimit{
            CommonFunction.showToast(msg: AlertMessage.youNeedToUpdateTwoScreenshots.localized)
            return
        }
        if self.imageArray.count == 0{
            CommonFunction.showToast(msg: AlertMessage.addAtleastOneImage.localized)
            return
        }
        updateTask()
    }
    
}
//MARK::- FUNCTION
extension UploadImageForMediaTaskVC{
    func onViewDidLoad(){
        if self.task?.action == TaskAction.setdp{
            self.imageLimit = 2
            lblSubtitle.text = AlertMessage.youHaveToAddTwoScreenshot.localized
            viewFooter.isHidden = true
        }
        if self.imageArray.count > 0{
            isUploadOption = false
        }
    }
}
//MARK::- IMAGE PICKER AND UPLOAD
extension UploadImageForMediaTaskVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagePicked = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.uploadImageToS3(image: imagePicked)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func uploadImageToS3(image: UIImage){
        CommonFunction.showLoader()
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            CommonFunction.hideLoader()
            if uploaded {
                self.imageArray.append(Image.init(url: imageUrl, size: self.imgSize.description))
                self.imgSize = 0
                self.isUploadOption = false
                self.tableView.reloadData()
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, size: {
            (val) in
            self.imgSize = val
        }, failure: { (err : Error) in
            CommonFunction.hideLoader()
                CommonFunction.showToast(msg: err.localizedDescription)
        })
    }
}

//MARK::- TABLE VIEW DELEGATE
extension UploadImageForMediaTaskVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.row == self.imageArray.count {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadImageCell") as? UploadImageCell else { fatalError("invalid cell \(self)")}
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadedCardCell") as? UploadedCardCell else { fatalError("invalid cell \(self)")}
                cell.btnCross.isHidden = false
                cell.configureCell(image: imageArray[indexPath.row], index: indexPath)
                cell.btnCross.tag = indexPath.row
                cell.btnCross.addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
                return cell
            }
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.task?.action == TaskAction.setdp{
            if self.imageArray.count == imageLimit{
                return imageLimit
            }
            return self.imageArray.count  + 1
        }else{
            return isUploadOption ? self.imageArray.count + 1 : self.imageArray.count
        }
        
    
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.imageArray.count{
            self.captureImage(on: self)
        }
    }
}


//MARK::- TARGETS
extension UploadImageForMediaTaskVC{
    @objc func deleteImage(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imageArray.remove(at: sender.tag)
        self.tableView.reloadData()
    }
}


//MARK::- API
extension UploadImageForMediaTaskVC{
        func updateTask(){
            var images = [String]()
            //images  = self.imageArray.map({$0.url})
            let _ = self.imageArray.map({ (image)  in
                images.append(image.url)
            })
            let image = images.joined(separator: ",")
            let params = [ApiKeys.taskId : /self.task?.task_id , ApiKeys.media : image]
            WebServices.updateTask(params: params , success: { [weak self](result) in
                self?.dismissVC(completion: {
                    self?.delegate?.success(arr: self?.imageArray ?? [])
                    if self?.task?.action == TaskAction.setdp{
                        CommonFunction.showToast(msg: AlertMessage.reviewTheTask.localized)
                    }
                })
            }) { (error) -> (Void) in
                CommonFunction.showToast(msg: error.localizedDescription)
            }
        }
    
}
