//
//  InnerCell2.swift
//  tableViewDemo
//
//  Created by naresh banavath on 14/07/20.
//  Copyright © 2020 naresh banavath. All rights reserved.
//

import UIKit

class InnerCell2: UITableViewCell {

    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var choice1Img: UIImageView!
    @IBOutlet weak var choice2Img: UIImageView!
    @IBOutlet weak var choice3Img: UIImageView!
    @IBOutlet weak var choice4Img: UIImageView!
    @IBOutlet weak var choice5Img: UIImageView!
    
    @IBOutlet weak var choice1View: UIView!
    @IBOutlet weak var choice2View: UIView!
    @IBOutlet weak var choice3View: UIView!
    @IBOutlet weak var choice4View: UIView!
    @IBOutlet weak var choice5View: UIView!
    
    @IBOutlet weak var choice1Label: UILabel!
    @IBOutlet weak var choice2Label: UILabel!
    @IBOutlet weak var choice3Label: UILabel!
    @IBOutlet weak var choice4Label: UILabel!
    @IBOutlet weak var choice5Label: UILabel!
    
    var isSubmitClicked:() -> (Void) = {}
       var selectedIndex : [String] = []
       var selectedOption: [String] = []

       var choice1Id = ""
       var choice2Id = ""
       var choice3Id = ""
       var choice4Id = ""
       var choice5Id = ""
       
       var isChoice1 = false
       var isChoice2 = false
       var isChoice3 = false
       var isChoice4 = false
       var isChoice5 = false
    
    @IBOutlet weak var pollSubmitBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pollSubmitBtn.addRightToLefttGradient()
        pollSubmitBtn.setCornerRadius(radius: 5.0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func pollsInitialization(polls:[PollOptionResult]){
           
           if polls.count == 1{
               choice1View.isHidden = false
               choice2View.isHidden = true
               choice3View.isHidden = true
               choice4View.isHidden = true
               choice5View.isHidden = true
               choice1Label.text = polls[0].optionname
               choice1Id = polls[0].option_id!
           }else if polls.count == 2{
               choice1View.isHidden = false
               choice2View.isHidden = false
               choice3View.isHidden = true
               choice4View.isHidden = true
               choice5View.isHidden = true
               choice1Label.text = polls[0].optionname
               choice2Label.text = polls[1].optionname
               choice1Id = polls[0].option_id!
               choice2Id = polls[1].option_id!
               
           }else if polls.count == 3{
               choice1View.isHidden = false
               choice2View.isHidden = false
               choice3View.isHidden = false
               choice4View.isHidden = true
               choice5View.isHidden = true
               choice1Label.text = polls[0].optionname
               choice2Label.text = polls[1].optionname
               choice3Label.text = polls[2].optionname
               choice1Id = polls[0].option_id!
               choice2Id = polls[1].option_id!
               choice3Id = polls[2].option_id!
           }else if polls.count == 4{
               choice1View.isHidden = false
               choice2View.isHidden = false
               choice3View.isHidden = false
               choice4View.isHidden = false
               choice5View.isHidden = true
               choice1Label.text = polls[0].optionname
               choice2Label.text = polls[1].optionname
               choice3Label.text = polls[2].optionname
               choice4Label.text = polls[3].optionname
               choice1Id = polls[0].option_id!
               choice2Id = polls[1].option_id!
               choice3Id = polls[2].option_id!
               choice4Id = polls[3].option_id!
               
           }else if polls.count == 5{
               choice1View.isHidden = false
               choice2View.isHidden = false
               choice3View.isHidden = false
               choice4View.isHidden = false
               choice5View.isHidden = false
               choice1Label.text = polls[0].optionname
               choice2Label.text = polls[1].optionname
               choice3Label.text = polls[2].optionname
               choice4Label.text = polls[3].optionname
               choice5Label.text = polls[4].optionname
               choice1Id = polls[0].option_id!
               choice2Id = polls[1].option_id!
               choice3Id = polls[2].option_id!
               choice4Id = polls[3].option_id!
               choice5Id = polls[4].option_id!
           }
       }
    
    @IBAction func choice1BtnAction(_ sender: Any) {
        
        if isChoice1 == false{
                   choice1Img.image = UIImage(named: "icSignupCheckbox")
                   isChoice1 = true
                   selectedOption.append(choice1Label.text!)
                   selectedIndex.append(choice1Id)
               }else{
                   choice1Img.image = UIImage(named: "icSignupDeselectCheckbox")
                   isChoice1 = false
                   selectedOption = selectedOption.filter{$0 != choice1Label.text!}
                   selectedIndex = selectedIndex.filter{$0 != choice1Id}
               }
        
    }
    @IBAction func choice2BtnAction(_ sender: Any) {
        
        if isChoice2 == false{
                   choice2Img.image = UIImage(named: "icSignupCheckbox")
                   isChoice2 = true
                   selectedOption.append(choice2Label.text!)
                   selectedIndex.append(choice2Id)
               }else{
                   choice2Img.image = UIImage(named: "icSignupDeselectCheckbox")
                   isChoice2 = false
                   selectedOption = selectedOption.filter{$0 != choice2Label.text!}
                   selectedIndex = selectedIndex.filter{$0 != choice2Id}
               }
    }
    @IBAction func choice3BtnAction(_ sender: Any) {
        
        if isChoice3 == false{
            choice3Img.image = UIImage(named: "icSignupCheckbox")
            isChoice3 = true
            selectedOption.append(choice3Label.text!)
            selectedIndex.append(choice3Id)
        }else{
            choice3Img.image = UIImage(named: "icSignupDeselectCheckbox")
            isChoice3 = false
            selectedOption = selectedOption.filter{$0 != choice3Label.text!}
            selectedIndex = selectedIndex.filter{$0 != choice3Id}
        }
    }
    @IBAction func choice4BtnAction(_ sender: Any) {
        
        if isChoice4 == false{
            choice4Img.image = UIImage(named: "icSignupCheckbox")
            isChoice4 = true
            selectedOption.append(choice4Label.text!)
            selectedIndex.append(choice4Id)
        }else{
            choice4Img.image = UIImage(named: "icSignupDeselectCheckbox")
            isChoice4 = false
            selectedOption = selectedOption.filter{$0 != choice4Label.text!}
            selectedIndex = selectedIndex.filter{$0 != choice4Id}
        }
    }
    @IBAction func choice5BtnAction(_ sender: Any) {
        if isChoice5 == false{
                   choice5Img.image = UIImage(named: "icSignupCheckbox")
                   isChoice5 = true
                   selectedOption.append(choice5Label.text!)
                   selectedIndex.append(choice5Id)
               }else{
                   choice5Img.image = UIImage(named: "icSignupDeselectCheckbox")
                   isChoice5 = false
                   selectedOption = selectedOption.filter{$0 != choice5Label.text!}
                   selectedIndex = selectedIndex.filter{$0 != choice5Id}
               }
        
    }
    @IBAction func submitClikc(_ sender: Any) {
        
        isSubmitClicked()

    }
    
}
