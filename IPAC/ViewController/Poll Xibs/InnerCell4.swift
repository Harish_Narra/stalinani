//
//  InnerCell4.swift
//  tableViewDemo
//
//  Created by naresh banavath on 14/07/20.
//  Copyright © 2020 naresh banavath. All rights reserved.
//

import UIKit

class InnerCell4: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var firstRadioBtn: UIButton!
    @IBOutlet weak var secondRadioBtn: UIButton!
    @IBOutlet weak var thirdRadioBtn: UIButton!
    @IBOutlet weak var fourthRadioBtn: UIButton!
    @IBOutlet weak var fifthRadioBtn: UIButton!
    @IBOutlet weak var pollSubmitBtn: UIButton!
    
    var isSubmitClicked:() -> (Void) = {}
    var selectedIndex = -1
    var selectedOption = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pollSubmitBtn.addRightToLefttGradient()
        pollSubmitBtn.setCornerRadius(radius: 5.0)
        
        // Initialization code
    }
    override func layoutIfNeeded() {
    
       // setGradientOnButton(btn: pollSubmitBtn)
          // setGradientOnLabel(btn: pollHeading)
          // setGradientOnView(btn: pollHeadingVw)
       }

    func ratePoll(rate1 : UIImage, rate2 : UIImage,rate3 : UIImage, rate4 : UIImage,rate5 : UIImage){
        firstRadioBtn.setImage(rate1, for: .normal)
        secondRadioBtn.setImage(rate2, for: .normal)
        thirdRadioBtn.setImage(rate3, for: .normal)
        fourthRadioBtn.setImage(rate4, for: .normal)
        fifthRadioBtn.setImage(rate5, for: .normal)

      
    }
    @IBAction func firstRateClicked(_ sender: Any) {
        selectedIndex = 0
               selectedOption = "1"
               ratePoll(rate1: UIImage(named: "icRadioBttn")!, rate2: UIImage(named: "icUnselectRadioBttn")!, rate3: UIImage(named: "icUnselectRadioBttn")!, rate4: UIImage(named: "icUnselectRadioBttn")!, rate5: UIImage(named: "icUnselectRadioBttn")!)
    }
    @IBAction func secondRateClicked(_ sender: Any) {
        
        selectedIndex = 1
        selectedOption = "2"
        ratePoll(rate1: UIImage(named: "icUnselectRadioBttn")!, rate2: UIImage(named: "icRadioBttn")!, rate3: UIImage(named: "icUnselectRadioBttn")!, rate4: UIImage(named: "icUnselectRadioBttn")!, rate5: UIImage(named: "icUnselectRadioBttn")!)
        
    }
    @IBAction func thirdrateClicked(_ sender: Any) {
        
        selectedIndex = 2
        selectedOption = "3"
        ratePoll(rate1: UIImage(named: "icUnselectRadioBttn")!, rate2: UIImage(named: "icUnselectRadioBttn")!, rate3: UIImage(named: "icRadioBttn")!, rate4: UIImage(named: "icUnselectRadioBttn")!, rate5: UIImage(named: "icUnselectRadioBttn")!)
    }
    @IBAction func fourthRateClicked(_ sender: Any) {
        
        selectedIndex = 3
        selectedOption = "4"
        ratePoll(rate1: UIImage(named: "icUnselectRadioBttn")!, rate2: UIImage(named: "icUnselectRadioBttn")!, rate3: UIImage(named: "icUnselectRadioBttn")!, rate4: UIImage(named: "icRadioBttn")!, rate5: UIImage(named: "icUnselectRadioBttn")!)
    }
    @IBAction func fifthrateClicked(_ sender: Any) {
        
        selectedIndex = 4
        selectedOption = "5"
        ratePoll(rate1: UIImage(named: "icUnselectRadioBttn")!, rate2: UIImage(named: "icUnselectRadioBttn")!, rate3: UIImage(named: "icUnselectRadioBttn")!, rate4: UIImage(named: "icUnselectRadioBttn")!, rate5: UIImage(named: "icRadioBttn")!)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func didTaponPollSubmit(_ sender: Any) {
        
        isSubmitClicked()
        pollSubmitBtn.backgroundColor = .gray
    }
    
}
