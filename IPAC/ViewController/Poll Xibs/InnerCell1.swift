//
//  InnerCell1.swift
//  tableViewDemo
//
//  Created by naresh banavath on 14/07/20.
//  Copyright © 2020 naresh banavath. All rights reserved.
//

import UIKit

class InnerCell1: UITableViewCell {
    
    var polloptionData2 : [PollOptionResult]?
    
    
    var isSubmitClicked:() -> (Void) = {}
    var selectedIndex = -1
    var selectedOption = ""

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var choice1View: UIView!
    @IBOutlet weak var choice2View: UIView!
    @IBOutlet weak var choice3View: UIView!
    @IBOutlet weak var choice4View: UIView!
    @IBOutlet weak var choice5View: UIView!
    
    @IBOutlet weak var choice1Label: UILabel!
    @IBOutlet weak var choice2Label: UILabel!
    @IBOutlet weak var choice3Label: UILabel!
    @IBOutlet weak var choice4Label: UILabel!
    @IBOutlet weak var choice5Label: UILabel!

    @IBOutlet weak var choice1Img: UIImageView!
    @IBOutlet weak var choice2Img: UIImageView!
    @IBOutlet weak var choice3Img: UIImageView!
    @IBOutlet weak var choice4Img: UIImageView!
    @IBOutlet weak var choice5Img: UIImageView!

    @IBOutlet weak var pollSubmitBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pollSubmitBtn.addRightToLefttGradient()
        pollSubmitBtn.setCornerRadius(radius: 5.0)
        
        // Initialization code
    }

    func pollsInitialization(polls:[PollOptionResult]){
        if polls.count == 1{
            choice1View.isHidden = false
            choice2View.isHidden = true
            choice3View.isHidden = true
            choice4View.isHidden = true
            choice5View.isHidden = true
            choice1Label.text = polls[0].optionname
        }else if polls.count == 2{
            choice1View.isHidden = false
            choice2View.isHidden = false
            choice3View.isHidden = true
            choice4View.isHidden = true
            choice5View.isHidden = true
            choice1Label.text = polls[0].optionname
            choice2Label.text = polls[1].optionname
        }else if polls.count == 3{
            choice1View.isHidden = false
            choice2View.isHidden = false
            choice3View.isHidden = false
            choice4View.isHidden = true
            choice5View.isHidden = true
            choice1Label.text = polls[0].optionname
            choice2Label.text = polls[1].optionname
            choice3Label.text = polls[2].optionname
        }else if polls.count == 4{
            choice1View.isHidden = false
            choice2View.isHidden = false
            choice3View.isHidden = false
            choice4View.isHidden = false
            choice5View.isHidden = true
            choice1Label.text = polls[0].optionname
            choice2Label.text = polls[1].optionname
            choice3Label.text = polls[2].optionname
            choice4Label.text = polls[3].optionname
        }else if polls.count == 5{
            choice1View.isHidden = false
            choice2View.isHidden = false
            choice3View.isHidden = false
            choice4View.isHidden = false
            choice5View.isHidden = false
            choice1Label.text = polls[0].optionname
            choice2Label.text = polls[1].optionname
            choice3Label.text = polls[2].optionname
            choice4Label.text = polls[3].optionname
            choice5Label.text = polls[4].optionname
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func choicePoll(choice1 : UIImage, choice2 : UIImage,choice3 : UIImage, choice4 : UIImage,choice5 : UIImage){
        choice1Img.image = choice1
        choice2Img.image = choice2
        choice3Img.image = choice3
        choice4Img.image = choice4
        choice5Img.image = choice5
    }
    
    
    @IBAction func choice1BtnClicked(_ sender: Any) {
        
        selectedIndex = 0
        selectedOption = "1"
        choicePoll(choice1: UIImage(named: "icRadioBttn")!, choice2: UIImage(named: "icUnselectRadioBttn")!, choice3: UIImage(named: "icUnselectRadioBttn")!, choice4: UIImage(named: "icUnselectRadioBttn")!, choice5: UIImage(named: "icUnselectRadioBttn")!)
    }
    @IBAction func choice2BtnClicked(_ sender: Any) {
        
        selectedIndex = 1
        selectedOption = "2"
        choicePoll(choice1: UIImage(named: "icUnselectRadioBttn")!, choice2: UIImage(named: "icRadioBttn")!, choice3: UIImage(named: "icUnselectRadioBttn")!, choice4: UIImage(named: "icUnselectRadioBttn")!, choice5: UIImage(named: "icUnselectRadioBttn")!)
        
    }
    @IBAction func choice3BtnClicked(_ sender: Any) {
        
        selectedIndex = 2
               selectedOption = "3"
               choicePoll(choice1: UIImage(named: "icUnselectRadioBttn")!, choice2: UIImage(named: "icUnselectRadioBttn")!, choice3: UIImage(named: "icRadioBttn")!, choice4: UIImage(named: "icUnselectRadioBttn")!, choice5: UIImage(named: "icUnselectRadioBttn")!)
        
    }
    @IBAction func choice4BtnClicked(_ sender: Any) {
        
        selectedIndex = 3
                     selectedOption = "4"
                     choicePoll(choice1: UIImage(named: "icUnselectRadioBttn")!, choice2: UIImage(named: "icUnselectRadioBttn")!, choice3: UIImage(named: "icUnselectRadioBttn")!, choice4: UIImage(named: "icRadioBttn")!, choice5: UIImage(named: "icUnselectRadioBttn")!)
              
    }
    @IBAction func choice5BtnClicked(_ sender: Any) {
        selectedIndex = 4
                            selectedOption = "5"
                            choicePoll(choice1: UIImage(named: "icUnselectRadioBttn")!, choice2: UIImage(named: "icUnselectRadioBttn")!, choice3: UIImage(named: "icUnselectRadioBttn")!, choice4: UIImage(named: "icUnselectRadioBttn")!, choice5: UIImage(named: "icRadioBttn")!)
    }
    
    @IBAction func didupdatePollClicked(_ sender: Any) {
        
         isSubmitClicked()
    }
}
