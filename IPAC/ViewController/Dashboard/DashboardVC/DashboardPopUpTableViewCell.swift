//
//  DashboardPopUpTableViewCell.swift
//  Stalinani
//
//  Created by HariTeju on 21/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class DashboardPopUpTableViewCell: UITableViewCell {

    @IBOutlet weak var profileIcon: AnimatableImageView!
    @IBOutlet weak var profileNum: UILabel!
    @IBOutlet weak var profileName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
