//
//  StaticPageVC.swift
//  IPAC
//
//  Created by macOS on 11/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit

enum WebViewType {
    
    case privacy
    case terms
    case faq
    case aboutus
    case none
}

var webViewType = WebViewType.none

class StaticPageVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var heading: UILabel!
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonFunction.showLoader()


        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    private func initialSetup() {
        
        self.webView.setCornerRadius(radius: 4.0)
        self.webView.layer.masksToBounds = true

        
        setupActivityIndicator()
        
        if webViewType == .privacy {
            self.urlString = "\(STATIC_PAGE_BASE_URL)privacy?lang=\(LocalizationSystem.sharedInstance.getLanguage())"
            self.heading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Privacy_Policy", comment: "")
            
        }else if webViewType == .terms {
            self.urlString = "\(STATIC_PAGE_BASE_URL)term?lang=\(LocalizationSystem.sharedInstance.getLanguage())"
            self.heading.text = "Terms"
        }else if webViewType == .faq {
            self.urlString = "\(STATIC_PAGE_BASE_URL)faq?lang=\(LocalizationSystem.sharedInstance.getLanguage())"
            self.heading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "FAQs", comment: "")
            //         print("NaadiRemember_me:", LocalizationSystem.sharedInstance.localizedStringForKey(key: "Remember_me", comment: ""))

            
        } else if webViewType == .aboutus {
            self.urlString = "\(STATIC_PAGE_BASE_URL)aboutUs?lang=\(LocalizationSystem.sharedInstance.getLanguage())"
            self.heading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "About_DMK", comment: "")
        }
        self.webView.navigationDelegate = self
        if let url =  URL(string: self.urlString){
            let request = URLRequest(url:url)
            
            self.webView.load(request)
        }
    }
    
    func setupActivityIndicator() {
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = AppColors.Blue.activityIndicator
        activityIndicator.center = webView.convert(webView.center, from: activityIndicator)
        webView.addSubview(activityIndicator)
    }
}

// MARK: -
extension StaticPageVC : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!){
      
        CommonFunction.showLoader()

       let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
       
       webView.evaluateJavaScript(jscript)
   }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         CommonFunction.hideLoader()
    }
   
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CommonFunction.hideLoader()
        CommonFunction.showToast(msg: error.localizedDescription)
        print("three")

    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        CommonFunction.hideLoader()
            CommonFunction.showToast(msg: error.localizedDescription)
        print("four")

        }
}
