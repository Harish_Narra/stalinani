//
//  EditProfilesVC.swift
//  IPAC
//
//  Created by macOS on 14/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class EditProfilesVC: UIViewController {

    // MARK:- IBProperties
    //====================
    
     let headingArray = ["",LocalizationSystem.sharedInstance.localizedStringForKey(key: "Social_Profiles", comment: ""),"ID Proof"]
    
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var navigationTitleLabel: UILabel!
    @IBOutlet weak var editProfileTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
}

// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
////==============================================
//extension EditProfilesVC : UITableViewDelegate, UITableViewDataSource {
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 3
//    }
//
//
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FooterViewid") as! FooterView
//        footer.backgroundColor = .clear
//        return footer
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 35
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderViewid") as! HeaderView
//        header.headingLabel.text = headingArray[section]
//        return header
//    }
//
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section{
//        case 0,1 :
//            return 5
//        default:
//            return 3
//        }
//    }
//}

// MARK:- FUNCTIONS
//====================
extension EditProfilesVC {
    
    /// Invite Page Content Initial Setup
    private func initialSetup() {
        
        navigationTitleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Profile_Edit", comment: "")
        super.addImageInBackground()
//        editProfileTableView.delegate = self
//        editProfileTableView.dataSource = self
        
        editProfileTableView.backgroundColor = .clear
        
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        editProfileTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderViewid")
        
        let footerNib = UINib.init(nibName: "FooterView", bundle: Bundle.main)
        editProfileTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "FooterViewid")
        
        editProfileTableView.register(UINib.init(nibName: "ProfileImageCell", bundle: nil), forCellReuseIdentifier: "profileImageCellid")
        editProfileTableView.register(UINib.init(nibName: "NameTextFieldCell", bundle: nil), forCellReuseIdentifier: "nameTextFieldCellid")
        editProfileTableView.register(UINib.init(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCellid")
        editProfileTableView.register(UINib.init(nibName: "UploadCell", bundle: nil), forCellReuseIdentifier: "UploadCellid")
    }
}

