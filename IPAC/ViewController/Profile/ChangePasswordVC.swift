//
//  ChangePasswordVC.swift
//  IPAC
//
//  Created by Appinventiv on 18/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ChangePasswordVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var txtOld: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNew: SkyFloatingLabelTextField!
    @IBOutlet weak var btnDone: GradientButton!
    @IBOutlet weak var changePassLabel: UILabel!
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
       
    }
    
    //MARK::- BUTTON ACTIONS
  
    @IBAction func btnActionChangePassword(_ sender: UIButton) {
        changePassword()
    }
    
    @IBAction func btnActionShowOld(_ sender: UIButton) {
     //   sender.isSelected.toggle()
     //   txtOld.isSecureTextEntry.toggle()
    }
    @IBAction func btnActionShowNew(_ sender: UIButton) {
      //  sender.isSelected.toggle()
      //  txtNew.isSecureTextEntry.toggle()
    }
    @IBAction func btnActionDismiss(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
}

//MARK::- FUNCTIONS
extension ChangePasswordVC{
    func onViewDidLoad(){
        
        changePassLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Change_Password", comment: "")
               txtOld.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Old_Password", comment: "")
               txtNew.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "New_Password", comment: "")
        self.btnDone.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Done", comment: ""), for: .normal)
        TFResponder.shared.addResponders([txtOld , txtNew])
        self.btnDone.dropShadow(color: UIColor.init(red: 79/255, green: 114/255, blue: 161/255, alpha:0.3), opacity: 1, offSet: CGSize.zero, radius: 10, scale: true)
        self.btnDone.rounded(cornerRadius: 5.0, clip: true)
    }
    func changePassword(){
        let oldPasswordValidation = ValidationController.validatePassword(password: /txtOld.text)
        let newPasswordValidation = ValidationController.validatePassword(password: /txtNew.text)
        if !oldPasswordValidation.0{
            CommonFunction.showToast(msg: oldPasswordValidation.1)
        }
        else if !newPasswordValidation.0{
            CommonFunction.showToast(msg: newPasswordValidation.1)
        }else{
            changePassworAPI()
        }
    }
}
//MARK::- API
extension ChangePasswordVC{
    func changePassworAPI(){
        WebServices.changePassword(old: /txtOld.text, new: txtNew.text, success: { [weak self](json) in
            CommonFunction.showToast(msg: json["MESSAGE"].stringValue)
            self?.dismissVC(completion: nil)

        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            return
        }
    }
}
