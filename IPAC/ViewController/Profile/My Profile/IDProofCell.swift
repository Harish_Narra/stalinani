//
//  IDProofCell.swift
//  IPAC
//
//  Created by Appinventiv on 31/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IDProofCell: UITableViewCell {

    @IBOutlet weak var idProofIcon: UIImageView!
    @IBOutlet weak var idTypeLabel: UILabel!
    @IBOutlet weak var IDNumberLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setLabel(index : Int? ,IdNum : String?)
    {
        switch index{
        case 1?:
            self.idTypeLabel.text = StringConstant.Voter_Card.localized
        case 2?:
            self.idTypeLabel.text = StringConstant.Pan_Card.localized
        case 3?:
            self.idTypeLabel.text = StringConstant.Driving_Licence.localized
        case 4?:
            self.idTypeLabel.text = StringConstant.Driving_Licence.localized
        default:
             self.idTypeLabel.text = "-"
        }
        if (IdNum ?? "").isEmpty {
            self.IDNumberLabel.text = "-"
        } else {
            self.IDNumberLabel.text = IdNum
        }
        
    }
}
