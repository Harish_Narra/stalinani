//
//  BaseVC.swift
//  IPAC
//
//  Created by Admin on 12/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    //    MARK:- Proporties
    //    =================
    //var firstRightBtn: UIBarButtonItem?
    //var secondRightBtn: UIBarButtonItem?
    
    //MARK:- ===============VIEW LIFE CYCLE===============
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    func setSideMenuButton(image: UIImage){
//
////        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.backButtonTapped))
//         self.navigationItem.leftBarButtonItem?.image = image
//    }
//
//    func setBackButton(image: UIImage = #imageLiteral(resourceName: "icSignupBack")){
//
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.backButtonTapped))
//        self.navigationController?.navigationBar.backIndicatorImage = image
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = image
//        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
//        self.additionAttributes()
//    }
    
//    @objc func backButtonTapped(){
//        _ = self.navigationController?.popViewController(animated: true)
//    }
//
//    func customiseNavigation(title: String){
//
//        self.navigationItem.title = title
//        //self.navigationItem.leftBarButtonItem?.title = ""
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: AppFonts.ProximaNova_Bold.withSize(15), NSAttributedStringKey.foregroundColor : AppColors.appThemeColor]
//        //self.additionAttributes()
//    }
//
//    private func additionAttributes(){
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.navigationBar.backgroundColor = .clear
//    }
//    
//    func setNavBarRightButtonItems(rightButtons: [UIBarButtonItem]){
//        self.navigationItem.rightBarButtonItems = rightButtons
//    }
//    
//    func setNavigationTitleView(titleView: UIView){
//        self.navigationController?.navigationItem.titleView = titleView
//    }
    //MARK::- FUNCTIONS
    func callToANumber(_ number : String = "9876543210") {
        if let url = URL(string: "tel://\(number)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
 
}



