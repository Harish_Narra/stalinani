//
//  UploadImageFooter.swift
//  IPAC
//
//  Created by Appinventiv on 23/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class UploadImageFooter: UITableViewHeaderFooterView {

    @IBOutlet weak var btnUploadFIle: UIButton!
    @IBOutlet weak var btnDone: AnimatableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        btnDone.addLeftToRightGradient()
    }

    
}
