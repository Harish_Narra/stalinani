//
//  MyWalletNewModelClass.swift
//  IPAC
//
//  Created by HariTeju on 12/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON



class MainSubclass : NSObject{
    
    var achievement : [MainAchievement]!
    var cODE : Int!
    var levels : [MainLevel]!
    var mESSAGE : String!
    var totalWalletAmount : String!
    var uSERINFO : MainUSERINFO!
    var valid : Bool!
    var wALLETDETAIL : MainWALLETDETAIL!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        achievement = [MainAchievement]()
        if let achievementArray = json["achievement"].array { self.achievement = achievementArray.map { MainAchievement(fromJson: $0) } }

        cODE = json["CODE"].intValue
        levels = [MainLevel]()
        if let levelsArray = json["levels"].array { self.levels = levelsArray.map { MainLevel(fromJson: $0) } }
        mESSAGE = json["MESSAGE"].stringValue
        totalWalletAmount = json["total_wallet_amount"].stringValue
        let uSERINFOJson = json["USERINFO"]
        if !uSERINFOJson.isEmpty{
            uSERINFO = MainUSERINFO(fromJson: uSERINFOJson)
        }
        valid = json["valid"].boolValue
        let wALLETDETAILJson = json["WALLET_DETAIL"]
        if !wALLETDETAILJson.isEmpty{
            wALLETDETAIL = MainWALLETDETAIL(json: wALLETDETAILJson)
        }
    }

}


class MainWALLETDETAIL : NSObject{
    
    var bonus : String!
    var expense : String!
    var reward : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    override init() {
        super.init()
    }
    required init(json : JSON) {
        
        bonus = json["bonus"].stringValue
        expense = json["expense"].stringValue
        reward = json["reward"].stringValue
    }

}


class MainUSERINFO : NSObject{
    
    var bankAccount : String!
    var defaultOption : String!
    var paytmNumber : String!
    var redeemableStatus : Int!
    var totalPoint : String!
    var updatedDate : String!
    var updatedExpenseDate : String!
    var upiAddress : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        bankAccount = json["bank_account"].stringValue
        defaultOption = json["default_option"].stringValue
        paytmNumber = json["paytm_number"].stringValue
        redeemableStatus = json["redeemable_status"].intValue
        totalPoint = json["total_point"].stringValue
        updatedDate = json["updated_date"].stringValue
        updatedExpenseDate = json["updated_expense_date"].stringValue
        upiAddress = json["upi_address"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    
        
    }
    



class MainLevel : NSObject{
    
    var achievement : [LevelAchievement]!
    var canRedeem : String!
    var descriptionField : String!
    var endPoint : String!
    var freeRewardPoints : String!
    var image : String!
    var incentive : String!
    var incentiveRedeemStatus : String!
    var isUnlocked : Bool!
    var levelTagName : String!
    var name : String!
    var nameTn : String!
    var pkLevelId : String!
    var startPoint : String!
    var unlockDate : String!
    var userLevelId : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        achievement = [LevelAchievement]()
        if let achievementArray = json["achievement"].array { self.achievement = achievementArray.map { LevelAchievement(fromJson: $0) } }

        canRedeem = json["can_redeem"].stringValue
        descriptionField = json["description"].stringValue
        endPoint = json["end_point"].stringValue
        freeRewardPoints = json["free_reward_points"].stringValue
        image = json["image"].stringValue
        incentive = json["incentive"].stringValue
        incentiveRedeemStatus = json["incentive_redeem_status"].stringValue
        isUnlocked = json["is_unlocked"].boolValue
        levelTagName = json["level_tag_name"].stringValue
        name = json["name"].stringValue
        nameTn = json["name_tn"].stringValue
        pkLevelId = json["pk_level_id"].stringValue
        startPoint = json["start_point"].stringValue
        unlockDate = json["unlock_date"].stringValue
        userLevelId = json["user_level_id"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    
    
}


class LevelAchievement : NSObject{
    
    var descriptionField : String!
    var eStatus : String!
    var fkLevelId : String!
    var image : String!
    var name : String!
    var pkAchievementId : String!
    var rewardPoints : String!
    var type : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        descriptionField = json["description"].stringValue
        eStatus = json["eStatus"].stringValue
        fkLevelId = json["fk_level_id"].stringValue
        image = json["image"].stringValue
        name = json["name"].stringValue
        pkAchievementId = json["pk_achievement_id"].stringValue
        rewardPoints = json["reward_points"].stringValue
        type = json["type"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    
    
}


class MainAchievement : NSObject{
    
    var descriptionField : String!
    var eStatus : String!
    var fkLevelId : String!
    var image : String!
    var isUnlocked : Bool!
    var levelName : String!
    var levelTagName : String!
    var name : String!
    var pkAchievementId : String!
    var rewardPoints : String!
    var type : String!
    var unlockDate : String!

    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        descriptionField = json["description"].stringValue
        eStatus = json["eStatus"].stringValue
        fkLevelId = json["fk_level_id"].stringValue
        image = json["image"].stringValue
        isUnlocked = json["is_unlocked"].boolValue
        levelName = json["level_name"].stringValue
        levelTagName = json["level_tag_name"].stringValue
        name = json["name"].stringValue
        pkAchievementId = json["pk_achievement_id"].stringValue
        rewardPoints = json["reward_points"].stringValue
        type = json["type"].stringValue
        unlockDate = json["unlock_date"].stringValue
    }
    

}

