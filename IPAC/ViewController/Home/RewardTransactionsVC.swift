//
//  RewardTransactionsVC.swift
//  IPAC
//
//  Created by HariTeju on 11/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import PullToRefreshKit
import IBAnimatable
    
class RewardTransactionsVC: BaseViewController2 {
    
    @IBOutlet weak var lblFooterNoData: UILabel!
    
    var walletData : ReferralModel?
    var arrData : [ReferralResult]?  = []
    var count = 0
    var status = 0 // 0 all , 1 earned  , 3 redeemed
    var startDateStr : String?
    var endDateStr : String?
    var startDate : Date?
    var endDate : Date?

    @IBOutlet weak var navigationTile: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTile.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reward_Transactions", comment: "")
        print("Testing")
        print("Testing")
  print("Testing")

        onViewDidLoads()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func filterClicked(_ sender: Any) {
        let filterVC = FilterVC.instantiate(fromAppStoryboard: .ContactUs)
        filterVC.toDateStr = endDateStr
        filterVC.fromDateStr =  startDateStr
        filterVC.toDate = endDate
        filterVC.fromDate =  startDate
        filterVC.selectedType = self.status
        filterVC.delegate = self
        self.presentVC(filterVC)
    }
    
}
extension RewardTransactionsVC : FilterDelegate{
    func apply(selectedType : Int , start : String? , end : String? ,startDate : Date? , endDate : Date? ){
        self.status = selectedType
        self.startDate = startDate
        self.endDate = endDate
        self.startDateStr = start
        self.endDateStr = end
        self.count = 0
        getDataFromAPI(loader : false)
        
    }
}

extension RewardTransactionsVC{
    func onViewDidLoads(){
        self.tableView.isHidden = true
//        headerView.addTapGesture { [weak self] (_) in
//            let detailVC = RewardDetailVC.instantiate(fromAppStoryboard: .ContactUs)
//            detailVC.walletData = self?.walletData
//            self?.presentVC(detailVC)
//        }
        self.tableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))
        configureTableView()
        handlePaging()
        getDataFromAPI(loader : true)
    }
    
    func setupUI(){
        self.tableView.switchRefreshFooter(to: .normal)
//        self.lblTotalEarning.text = self.walletData?.userInfo?.total_point.toInt()?.description ?? "0"
//        self.lblTotalRedeemable.text = self.walletData?.walletDetail?.expense.toInt()?.description ?? "0"
        for item in self.walletData?.result ?? []{
            self.arrData?.append(item)
        }
        if count == 0{
            self.arrData = self.walletData?.result ?? []
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.count = self.walletData?.count ?? -1
        self.tableViewDataSource?.items = self.arrData
        self.tableView.reloadData()
        self.tableView.isHidden = false
        self.lblFooterNoData.isHidden = !(self.arrData?.isEmpty ?? true)
        if let dateReward = formatter.date(from: /self.walletData?.userInfo?.updated_date) {
            formatter.dateFormat = "d MMMM yyyy"
           // self.lblLastUpdateEarning.text = formatter.string(from: dateReward)
        }
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let dateExpense = formatter.date(from: /self.walletData?.userInfo?.updated_expense_date) {
            formatter.dateFormat = "d MMMM yyyy"
           // self.lblLastUpdateRedeemable.text = formatter.string(from: dateExpense)
        }
    }
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.count = 0
        self.getDataFromAPI(loader : false)
    }
}
extension RewardTransactionsVC{
    func configureTableView(){
        tableViewDataSource = TableViewCustomDatasource.init(items: self.arrData , height: UITableViewAutomaticDimension, estimatedHeight: 40, tableView: tableView, cellIdentifier: Cells.RewardtransactionsCell.rawValue, configureCellBlock: { (cell, item, indexpath) in
            guard let cell = cell as? RewardtransactionsCell , let item = item as? ReferralResult else {return}
            cell.configure(item : item)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            guard let date = formatter.date(from: /self.arrData?[indexpath.row].created_date) else { return }
            if indexpath.row == 0 {
                cell.lblDate.isHidden = false
            }else{
                guard let prevDate = formatter.date(from: /self.arrData?[indexpath.row - 1].created_date) else { return }
                cell.lblDate.isHidden = Calendar.current.isDate(date, inSameDayAs: prevDate)
            }
        }, aRowSelectedListener: nil, willDisplayCell: nil)
    }
}
extension RewardTransactionsVC{
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        tableView.configRefreshFooter(with: footer, container: self) {
            self.count == -1 ? self.tableView.switchRefreshFooter(to: .normal) :  self.getDataFromAPI(loader : false)
        }
    }
}
extension RewardTransactionsVC {
    
    func getDataFromAPI(loader : Bool){
    //0 all , 1 reward redeemed , 2 reward earned
    var tempStat = 0
    if status == 2{
    tempStat = 1
    }else if status == 1{
    tempStat = 3
    
    }
    
    WebServices.getWallet(dict : ["count" : count , "status" : tempStat , "start_date" : /startDateStr , "end_date" : /endDateStr],loader
    , success: { [weak self] (json) in
    self?.walletData = ReferralModel.init(json: json)
    self?.setupUI()
    
    }) {(error) -> (Void) in
    CommonFunction.showToast(msg: error.localizedDescription)
    }
    }
}
