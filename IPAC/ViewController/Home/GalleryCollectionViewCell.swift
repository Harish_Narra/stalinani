//
//  GalleryCollectionViewCell.swift
//  IPAC
//
//  Created by HariTeju on 06/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var firstVerticalStackView: UIStackView!
    @IBOutlet weak var firstTopImage: UIImageView!
    @IBOutlet weak var firstBottomImage: UIImageView!
    @IBOutlet weak var secondVerticalStackView: UIStackView!
    @IBOutlet weak var secondTopImage: UIImageView!
    @IBOutlet weak var secondBottomContainerView: UIView!
    @IBOutlet weak var secondBottomImage: UIImageView!
    @IBOutlet weak var secondBottomOverlayView: UIView!
    @IBOutlet weak var secondBottomImageCountLabel: UILabel!
    @IBOutlet weak var horizontalStackViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        firstTopImage.clipsToBounds = true
        firstBottomImage.clipsToBounds = true
        secondTopImage.clipsToBounds = true
        secondBottomImage.clipsToBounds = true
        
        showAllImageAndStacks()

    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        containerView.rounded(cornerRadius: 5.0, clip: true)
    }
    func showAllImageAndStacks() {
        secondVerticalStackView.isHidden = false
        firstBottomImage.isHidden = false
        secondBottomContainerView.isHidden = false
        secondBottomOverlayView.isHidden = false
        firstTopImage.isHidden = false
        secondTopImage.isHidden = false
        firstVerticalStackView.isHidden = false
    }
    
}

