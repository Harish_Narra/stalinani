//
//  GalleryCellTableViewCell.swift
//  IPAC
//
//  Created by macOS on 31/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class GalleryCellTableViewCell: UITableViewCell {

    //MARK:- IBOutlets
    //==================
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var firstVerticalStackView: UIStackView!
    @IBOutlet weak var firstTopImage: UIImageView!
    @IBOutlet weak var firstBottomImage: UIImageView!
    @IBOutlet weak var secondVerticalStackView: UIStackView!
    @IBOutlet weak var secondTopImage: UIImageView!
    @IBOutlet weak var secondBottomContainerView: UIView!
    @IBOutlet weak var secondBottomImage: UIImageView!
    @IBOutlet weak var secondBottomOverlayView: UIView!
    @IBOutlet weak var secondBottomImageCountLabel: UILabel!
    @IBOutlet weak var horizontalStackViewHeight: NSLayoutConstraint!
    
    
    //MARK:- Properties
    //====================
    override func awakeFromNib() {
        super.awakeFromNib()

        firstTopImage.clipsToBounds = true
        firstBottomImage.clipsToBounds = true
        secondTopImage.clipsToBounds = true
        secondBottomImage.clipsToBounds = true
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        containerView.rounded(cornerRadius: 5.0, clip: true)
    }
    
    func showAllImageAndStacks() {
        secondVerticalStackView.isHidden = false
        firstBottomImage.isHidden = false
        secondBottomContainerView.isHidden = false
        secondBottomOverlayView.isHidden = false
        firstTopImage.isHidden = false
        secondTopImage.isHidden = false
        firstVerticalStackView.isHidden = false
    }
    
    
    func populate(with data: News) {
        
        showAllImageAndStacks()
        guard let mediaSet = data.mediaSet else { return }
        
        switch mediaSet.count {
            
        case 0:
            horizontalStackViewHeight.constant = 0
            layoutIfNeeded()
        case 1:
            secondVerticalStackView.isHidden = true
            firstBottomImage.isHidden = true            
            self.firstTopImage.kf.setImage(with: URL(string: mediaSet[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
        case 2:
            firstBottomImage.isHidden = true
            secondBottomContainerView.isHidden = true
            
            self.firstTopImage.kf.setImage(with: URL(string: mediaSet[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.secondTopImage.kf.setImage(with: URL(string: mediaSet[1].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
        case 3:
            firstBottomImage.isHidden = true
           
            
            self.firstTopImage.kf.setImage(with: URL(string: mediaSet[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.secondTopImage.kf.setImage(with: URL(string: mediaSet[1].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.secondBottomImage.kf.setImage(with: URL(string: mediaSet[2].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            
            secondBottomOverlayView.isHidden = true
            
        case 4:
            self.firstTopImage.kf.setImage(with: URL(string: mediaSet[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.firstBottomImage.kf.setImage(with: URL(string: mediaSet[1].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.secondTopImage.kf.setImage(with: URL(string: mediaSet[2].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.secondBottomImage.kf.setImage(with: URL(string: mediaSet[3].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            
            secondBottomOverlayView.isHidden = true
         
        default:
            self.firstTopImage.kf.setImage(with: URL(string: mediaSet[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.firstBottomImage.kf.setImage(with: URL(string: mediaSet[1].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.secondTopImage.kf.setImage(with: URL(string: mediaSet[2].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            self.secondBottomImage.kf.setImage(with: URL(string: mediaSet[3].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            
            secondBottomImageCountLabel.text = "+\(mediaSet.count - 4)"
        }
        titleLabel.text = data.newsTitle
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date =  formatter.date(from: data.createdDate ?? "") else { return }
        let displayDate = date.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: TimeZone.current)
        self.dateLabel.text = displayDate
        
        let rawText = (data.newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        do {
            let attrStr = try NSAttributedString(
                data: rawText,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            descriptionLabel.attributedText = attrStr
            
        } catch {
            print_debug(error.localizedDescription)
        }
    }
    
}
