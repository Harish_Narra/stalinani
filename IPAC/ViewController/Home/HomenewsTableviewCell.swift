//
//  HomenewsTableviewCell.swift
//  IPAC
//
//  Created by HariTeju on 05/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class HomenewsTableviewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
  //  var news = [News]()
   // var news2 = [News]()

    var news: [News] = []
    var newspost: [News] = []


    @IBOutlet weak var pagesss: UIPageControl!
    
    //var newsarray = [News]
 
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       // self.getHomeNewss(count: 0, loader: false)

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false

        collectionView.layoutIfNeeded()
        
      
        
//
//        for (index, name) in self.news.enumerated() {
//
//        }

        
//        for i in 0..< self.news.count {
//            if self.news[i].newsCategory == "" {
//
//            }
//        }
        
       
//        let cellWidth : CGFloat = collectionView.frame.size.width / 4.0
//        let cellheight : CGFloat = collectionView.frame.size.height - 2.0
//        let cellSize = CGSize(width: cellWidth , height:cellheight)
//
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .vertical //.horizontal
//        layout.itemSize = cellSize
//        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
//        layout.minimumLineSpacing = 1.0
//        layout.minimumInteritemSpacing = 1.0
//        collectionView.setCollectionViewLayout(layout, animated: true)
        
        collectionView.reloadData()
        
//        if self.newspost.count == 0 {
//            HomenewsTableviewCell.tableFooterView = UIView()
//
//        }
        // Initialization code
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("self.newspost.count", self.newspost.count)
                return self.newspost.count

    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pagesss.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeNewsCollectionViewCell", for: indexPath) as? HomeNewsCollectionViewCell else{
            fatalError() }
        
        cell.titleLabel.text = self.newspost[indexPath.row].newsTitle ?? ""
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date =  formatter.date(from: self.newspost[indexPath.row].createdDate ?? "") 
        let displayDate = date?.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: .current)
        cell.timeLabel.text = displayDate
 
        let data = (self.newspost[indexPath.row].newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        cell.descriptionLabel.text = attrStr?.string
         if newspost[indexPath.row].mediaSet?.count == 0 {
            
            cell.imageOrVideo.image = UIImage.init(named: "placeHolder")
            cell.playButton.setImage(nil, for: .normal)

                   
               } else {
                 //  cell.imageOrVideo.image = UIImage.init(named: #imageLiteral(resourceName: "placeHolder"))
                  // cell.imageOrVideo.kf.setImage(with: URL(string: mediaSet?.mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
                   let mediaSet = newspost[indexPath.row].mediaSet?[0]
                   if mediaSet?.mediaType == "2" {
                       cell.playButton.setImage(#imageLiteral(resourceName: "playIcon"), for: .normal)
                       cell.imageOrVideo.kf.setImage(with: URL(string: mediaSet?.mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
                   }else{
                       cell.imageOrVideo.kf.setImage(with: URL(string: mediaSet?.mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
                       cell.playButton.setImage(nil, for: .normal)
                      // btnPlay.setImage(nil, for: .normal)
                   }

               }
        
        

        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(self.newspost[indexPath.row])
        
        if self.newspost[indexPath.row].mediaSet?.count == 0 {
            
            let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
            bannerSceen.news = self.newspost[indexPath.row]
            self.parentViewController?.pushVC(bannerSceen)
        } else {
            
        
        
    //    print(/self.newspost[indexPath.row].mediaSet?[0].mediaType)
        
                 //   self.navigationController?.pushViewController(bannerSceen, animated: true)
        if /self.newspost[indexPath.row].mediaSet?[0].mediaType == "2" {
//            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
//            vc.url = /self.newspost[indexPath.row].mediaSet?[0].mediaUrl
//            vc.desc = /self.newspost[indexPath.row].newsTitle
//            ez.topMostVC?.presentVC(vc)
            let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
            bannerSceen.news = self.newspost[indexPath.row]
            self.parentViewController?.pushVC(bannerSceen)
        } else {
//            let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
//            previewVC.previewImage = /self.newspost[indexPath.row].mediaSet?[0].mediaUrl
//            previewVC.tittle = self.newspost[indexPath.row].newsTitle ?? ""
//            self.parentViewController?.pushVC(previewVC)
            let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
            bannerSceen.news = self.newspost[indexPath.row]
            self.parentViewController?.pushVC(bannerSceen)
        }
        }
    }
//    func collectionView(collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAtIndexPath indexPath:  NSIndexPath) -> CGSize {
//
//        return CGSize(width: 351, height:341)
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func getHomeNewss(count: Int, loader: Bool , _ refreshControl : UIRefreshControl? = nil){
        print_debug("check api")
        WebServices.getHomeData(params: [ApiKeys.count: count], loader: loader, success: { [weak self] (json) in
            refreshControl?.endRefreshing()
            //self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
           // self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            var news = [News]()
            for obj in json[ApiKeys.result].arrayValue {
                let newsObj = News(json: obj)
                news.append(newsObj)
            }
            var priorityNews = [News]()
            for obj in json["PRIORITY_NEWS"].arrayValue {
                let newsObj = News(json: obj)
                priorityNews.append(newsObj)
            }
            if count == 0{
                self?.news = priorityNews
                self?.news.append(contentsOf: news)
            }else{
                self?.news.append(contentsOf: news)
            }
            
            print("self.news.count", self?.news.count ?? 0)
            // print(self.news.count)
//            for i in self!.news   {
//
//                print("iiiiii:" , i)
//
//            }
            
            if self?.news.count == 0 {
                           
                       } else {
            for (index, element) in self!.news.enumerated() {
                print("Item \(index): \(element)")
                print(self?.news[index].newsCategory ?? "")
                
                if (self?.news[index].newsCategory)!.rawValue == "post" || (self?.news[index].newsCategory)!.rawValue == "article"{
                    
                    print("self.minnews", self?.news[index] ?? "")
                    
                    //self?.newspost =
                    self?.newspost.append((self?.news[index])!)
                    
                   // newspost = newspost.
                    
//                    newsarray = newsarray.append((self?.news[index])!)
                    
                }
                }

            }
            self?.pagesss.numberOfPages = self?.newspost.count ?? 0

            self?.collectionView.reloadData()
            
//            self?.lblNoData.isHidden = !(self?.news.isEmpty ?? true)
//            self?.reloadWithoutAnimation()
//            self?.count = json[ApiKeys.next].intValue
//            self?.checkBonus(json : json)
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            refreshControl?.endRefreshing()
//            self.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
//            self.lblNoData.isHidden = !(self.news.isEmpty )
            return
        }
    }

}
//extension HomenewsTableviewCell {
//
//    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
//
//        collectionView.delegate = dataSourceDelegate
//        collectionView.dataSource = dataSourceDelegate
//        collectionView.tag = row
//        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
//        collectionView.reloadData()
//    }
//
//    var collectionViewOffset: CGFloat {
//        set { collectionView.contentOffset.x = newValue }
//        get { return collectionView.contentOffset.x }
//    }
//}

