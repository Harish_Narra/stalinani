//
//  StoryVC.swift
//  IPAC
//
//  Created by macOS on 31/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class StoryVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlets
    @IBOutlet weak var storyTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    //MARK:-
    //MARK:- Properties
    var news: News?
    
    //MARK:-
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

       self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    //MARK:- Targets
    
}

//MARK:-
//MARK:- Table view Delegate and DataSource
extension StoryVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         guard let mediaSet = news?.mediaSet else { return 0 }
        return mediaSet.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "StoryHeaderCellTableViewCell") as! StoryHeaderCellTableViewCell
        guard let news = news else { return UIView() }
        header.populateHeaderData(data: news)
        header.btnReadMore.isUserInteractionEnabled = true
        header.btnReadMore.addTarget(self, action: #selector(self.readMore(_:)), for: .touchUpInside)
        return header
    }
    @objc func readMore(_ sender: UIButton) {
        let vc = WebPageVC.instantiate(fromAppStoryboard: .Form)
        vc.url = /self.news?.url
        //vc.titleHead = ""
        self.navigationController?.pushViewController(vc, animated: false)
        //gotoweb
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryImageCell") as? GalleryImageCell else { fatalError("invalid cell \(self)")
        }
        guard let news = news else { fatalError("empty news model") }
        cell.populateCell(with: news, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         guard let mediaSet = news?.mediaSet else { return }
        let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
        previewVC.previewImage = mediaSet[indexPath.row].mediaUrl ?? ""
        previewVC.tittle = news?.newsTitle ?? ""
        self.navigationController?.pushViewController(previewVC, animated: true)
    }
}

//MARK:-
//MARK:- Private Methods
private extension StoryVC {

    func initialSetup() {
        storyTableView.delegate = self
        storyTableView.dataSource = self
        
        setupText()
        registerXib()
    }
    
    func setupText() {
        self.titleLabel.text = StringConstant.Story.localized
    }
    
    func registerXib() {
        let headerNib = UINib.init(nibName: "StoryHeaderCellTableViewCell", bundle: Bundle.main)
        storyTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "StoryHeaderCellTableViewCell")
    }
}

//MARK:- Prototype Cell
//=========================
class GalleryImageCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var bgView: UIView!
    //@IBOutlet weak var thirdImage: UIImageView!
    //@IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    //  @IBOutlet weak var imageFour: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       // secondImage.isHidden = true
        //thirdImage.isHidden = true
        //imageFour.isHidden = true
        setImages()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        bgView.rounded(cornerRadius: 5.0, clip: true)
    }
    
    func setImages() {
        firstImage.clipsToBounds = true
        //secondImage.clipsToBounds = true
        //thirdImage.clipsToBounds = true
        //imageFour.clipsToBounds = true
    }
    
    func populateCell(with data: News, indexPath: IndexPath) {
       guard let mediaSet = data.mediaSet else { return }
        self.firstImage.kf.setImage(with: URL(string: mediaSet[indexPath.row].mediaUrl ?? ""), placeholder: #imageLiteral(resourceName: "icHomeImageerror"))
    }
}
