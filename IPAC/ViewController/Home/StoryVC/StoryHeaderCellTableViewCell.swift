//
//  StoryHeaderCellTableViewCell.swift
//  IPAC
//
//  Created by macOS on 31/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class StoryHeaderCellTableViewCell: UITableViewHeaderFooterView {

   
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var btnReadMore: GradientButton!
    @IBOutlet weak var heightReadMore: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populateHeaderData( data : News) {
        titleLabel.text = data.newsTitle
        let formatter = DateFormatter()
        btnReadMore.isHidden = /data.url == ""
        heightReadMore.constant = /data.url == "" ? 0 :  32

        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date =  formatter.date(from: data.createdDate ?? "") else { return }
        let displayDate = date.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: TimeZone.autoupdatingCurrent)
        self.dateLabel.text = displayDate
        
        let rawText = (data.newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        do {
            let attrStr = try NSAttributedString(
                data: rawText,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            descLabel.attributedText = attrStr
            
        } catch {
            print_debug(error.localizedDescription)
        }
    }
   
}
