//
//  GalleryTableViewCell.swift
//  IPAC
//
//  Created by HariTeju on 06/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class GalleryTableViewCell: UITableViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pagessss: UIPageControl!
    
    var news: [News] = []
    var newspost: [News] = []
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       // self.getHomeNewss(count: 0, loader: false)
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.layoutIfNeeded()
        collectionView.reloadData()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
                print("self.newspost.countssssss", self.newspost.count)
        //        //return 2
        return self.newspost.count
        //  return 3
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pagessss.currentPage = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as? GalleryCollectionViewCell else{
            fatalError() }
        
        print("rtyryrtyr", self.newspost[indexPath.row].mediaSet?.count ?? 0)
        
        
       // guard self.newspost[indexPath.row].mediaSet else { return }
//
        switch self.newspost[indexPath.row].mediaSet?.count ?? 0 {

        case 0:
            cell.horizontalStackViewHeight.constant = 0
            layoutIfNeeded()
        case 1:
            cell.secondVerticalStackView.isHidden = true
            cell.firstBottomImage.isHidden = true
            cell.firstTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
        case 2:
            cell.firstBottomImage.isHidden = true
            cell.secondBottomContainerView.isHidden = true

            cell.firstTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.secondTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[1].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
        case 3:
            cell.firstBottomImage.isHidden = true


            cell.firstTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.secondTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[1].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.secondBottomImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[2].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))

            cell.secondBottomOverlayView.isHidden = true

        case 4:
            cell.firstTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.firstBottomImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[1].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.secondTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[2].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.secondBottomImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[3].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))

            cell.secondBottomOverlayView.isHidden = true

        default:
            cell.firstTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.firstBottomImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[1].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.secondTopImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[2].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            cell.secondBottomImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[3].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))

            cell.secondBottomImageCountLabel.text = "+\(self.newspost[indexPath.row].mediaSet?.count ?? 0 - 4)"
        }
        
        
        cell.titleLabel.text = self.newspost[indexPath.row].newsTitle
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         let date =  formatter.date(from: self.newspost[indexPath.row].createdDate ?? "")
        let displayDate = date?.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: TimeZone.current)
        cell.dateLabel.text = displayDate
        
        let rawText = (self.newspost[indexPath.row].newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        do {
            let attrStr = try NSAttributedString(
                data: rawText,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            cell.descriptionLabel.attributedText = attrStr
            
        } catch {
            print_debug(error.localizedDescription)
        }
        
        
//        cell.btnRegister.isHidden = self.newspost[indexPath.row].is_completed != "0"
//        let data = (self.newspost[indexPath.row].newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
//        let attrStr = try? NSAttributedString( // do catch
//            data: data,
//            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
//            documentAttributes: nil)
//        cell.lblDesc.attributedText = attrStr
//
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storySceen = StoryVC.instantiate(fromAppStoryboard: .Home)
        storySceen.news = self.newspost[indexPath.row]
        self.parentViewController?.pushVC(storySceen)

    }
    
    
    func getHomeNewss(count: Int, loader: Bool , _ refreshControl : UIRefreshControl? = nil){
        print_debug("check api")
        WebServices.getHomeData(params: [ApiKeys.count: count], loader: loader, success: { [weak self] (json) in
            refreshControl?.endRefreshing()
            //self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            // self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            var news = [News]()
            for obj in json[ApiKeys.result].arrayValue {
                let newsObj = News(json: obj)
                news.append(newsObj)
            }
            var priorityNews = [News]()
            for obj in json["PRIORITY_NEWS"].arrayValue {
                let newsObj = News(json: obj)
                priorityNews.append(newsObj)
            }
            if count == 0{
                self?.news = priorityNews
                self?.news.append(contentsOf: news)
            }else{
                self?.news.append(contentsOf: news)
            }
            
            print("self.news.count", self?.news.count ?? 0)
            // print(self.news.count)
//            for i in self!.news   {
//                
//                print("iiiiii:" , i)
//                
//            }
            if self?.news.count == 0 {
                           
                       } else {
            for (index, element) in self!.news.enumerated() {
                print("Item \(index): \(element)")
                print(self?.news[index].newsCategory ?? "")
                
                if (self?.news[index].newsCategory)!.rawValue == "gallery"{
                    
                    print("self.minnews", self?.news[index] ?? "")
                    
                    //self?.newspost =
                    self?.newspost.append((self?.news[index])!)
                    
                    // newspost = newspost.
                    
                    //                    newsarray = newsarray.append((self?.news[index])!)
                    
                    
                }
                }
                
            }
            self?.pagessss.numberOfPages = self?.newspost.count ?? 0

            self?.collectionView.reloadData()
            
            //            self?.lblNoData.isHidden = !(self?.news.isEmpty ?? true)
            //            self?.reloadWithoutAnimation()
            //            self?.count = json[ApiKeys.next].intValue
            //            self?.checkBonus(json : json)
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            refreshControl?.endRefreshing()
            //            self.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            //            self.lblNoData.isHidden = !(self.news.isEmpty )
            return
        }
    }
}
