//
//  ReferralVC.swift
//  IPAC
//
//  Created by Appinventiv on 17/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import PullToRefreshKit


class ReferralVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnSideMenu: MenuButton!
    @IBOutlet weak var recentLabel: UILabel!
    @IBOutlet weak var txtReferral: UITextField!
    @IBOutlet weak var lblReferralCodeText: UILabel!
    @IBOutlet weak var navigationLabelReferal: UILabel!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var heightFooterView: NSLayoutConstraint!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var viewTapReferral: UIView!
    
    @IBOutlet weak var shareebtn: UIButton!
    @IBOutlet weak var yourReferalJustLabel: UILabel!
    //MARK::- PROPERTIES
    var referData : ReferralModel?
    var arrData : [ReferralResult]?  = []
    var count = 0
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        onDidLayoutSubviews()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionShare(_ sender: UIButton) {
        shareReferralCode()
    }
    
}

//MARK::- FUNCTIONS
extension ReferralVC{
    func onViewDidLoad(){
        
        //        print("newnaddi: ", LocalizationSystem.sharedInstance.localizedStringForKey(key: "Unique_ID_Number", comment: ""))
//        "Share" = "பகிர்";
//        "Share your referral code" = "உங்களது பரிந்துரை குறியீட்டை பகிர்க"
//        "with your friends to help them gain unprecedented campaign experience, contribute meaningfully to spread their leader\'s vision and avail scores of added benefits and opportunities!!!" = "உங்களது பரிந்துரை குறியீடை நண்பர்களுடன் பகிர்ந்து அவர்கள் இதுவரை இல்லாத பிரச்சார அனுபவம் பெறுவதோடு, உங்களது தலைவரின் நோக்கத்திற்கு அர்த்தமுள்ள பங்களிப்பை தரலாம். நன்மைகளையும் வாய்ப்புகளையும் சேர்த்தே பெருங்கள்.";
//        "Your referral" = "உங்களுடைய பரிந்துரை குறியீடு";
//        "Recents" = "சமீபத்திய";
//        "Total" = "மொத்தம்";
//        "Referral" = "பரிந்துரை";
        navigationLabelReferal.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Referral", comment: "")
        
        totalLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Tota", comment: "")

        shareebtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Share", comment: ""), for: .normal)
        yourReferalJustLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Your referral", comment: "")
        recentLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Recents", comment: "")
        
        self.viewHeader.isHidden = false
        self.heightFooterView.constant = 40
        self.tableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208, target: self, selector: #selector(pullToRefresh(_:)))
        configureTable()
        handlePaging()
        getDataFromAPI(loader : true)
        self.viewTapReferral.addTapGesture {[weak self] (_) in
            UIPasteboard.general.string = self?.txtReferral.text
            CommonFunction.showToast(msg: Constants.textCopied.rawValue.localized)
        }
    }
    
    func shareReferralCode(){
        let someText:String =  /self.referData?.userInfo?.referal_content.htmlToString + Constants.referralMsg.rawValue.localized + /txtReferral.text
        let activityViewController = UIActivityViewController(activityItems : [someText], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func setupUI(){
        self.tableView.switchRefreshFooter(to: .normal)
        self.tableView.isHidden = false
        self.txtReferral.text = /self.referData?.userInfo?.referal_code
        lblReferralCodeText.text = "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Share your referral code", comment: "")) \(/referData?.userInfo?.referal_code) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "with your friends to help them gain unprecedented campaign experience, contribute meaningfully to spread their leader\'s vision and avail scores of added benefits and opportunities!!!", comment: ""))"
        self.arrData?.append(contentsOf : self.referData?.result ?? [])
        self.lblTotalCount.text = "Total : \(/self.referData?.totalReferral)"
        self.viewHeader.isHidden = self.count == -1
        self.heightFooterView.constant = self.count == -1 ? 40 : 0
        self.count = self.referData?.count ?? -1
        self.tableViewDataSource?.items = self.arrData
        self.tableView.reloadData()
    }
    @objc func pullToRefresh(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.count = 0
        self.getDataFromAPI(loader: false)
    }
}

//MARK::- CONFIGURE TABLEVIEW
extension ReferralVC{
    func configureTable(){
        tableViewDataSource = TableViewCustomDatasource.init(items: self.arrData , height: UITableViewAutomaticDimension, estimatedHeight: 40, tableView: tableView, cellIdentifier: Cells.ReferralTableViewCell.rawValue, configureCellBlock: { (cell, item, indexpath) in
            guard let cell = cell as? ReferralTableViewCell , let item = item as? ReferralResult else {return}
            cell.configure(item : item)
            }, aRowSelectedListener: nil, willDisplayCell: nil)
    }
}

//MARK::- API
extension ReferralVC{
    func getDataFromAPI(loader : Bool){
        WebServices.referralList(count: self.count.description,loader
            , success: { [weak self] (json) in
                self?.referData = ReferralModel.init(json: json)
                self?.setupUI()
                
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}


//MARK::- PAGINATION
extension ReferralVC{
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        tableView.configRefreshFooter(with: footer, container: self) {
            self.count == -1 ? self.tableView.switchRefreshFooter(to: .normal) :  self.getDataFromAPI(loader : false)
        }
    }
}


