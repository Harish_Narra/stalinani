//
//  KnowledgeHomeCollectionViewCell.swift
//  Stalinani
//
//  Created by HariTeju on 29/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class KnowledgeHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var seemoreBtn: UIButton!
}
