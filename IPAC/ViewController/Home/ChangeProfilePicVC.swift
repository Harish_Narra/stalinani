//
//  ChangeProfilePicVC.swift
//  IPAC
//
//  Created by macOS on 30/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
protocol UpdateView: class {
  
    func reloadViewsData()
}

class ChangeProfilePicVC: BaseVC {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    weak var delegate: UpdateView?
  
    override func viewDidLoad() {
        super.viewDidLoad()

       initialSetup()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addImagebtnTapped(_ sender: UIButton) {
        self.captureImage(on: self)
    }
    
}

// MARK:- Private Methods
//=========================
private extension ChangeProfilePicVC {
    func initialSetup() {
        self.profileImageView.kf.setImage(with: URL(string : /CommonFunction.getUser()?.userImage), placeholder: #imageLiteral(resourceName: "icPlaceholder"), options: nil, progressBlock: nil, completionHandler: nil)

    }
    
    func updateImage(imageUrl: String) {
        WebServices.updateProfile(param: [ApiKeys.profileImage: imageUrl], loader: true, success: { (json) in
            print_debug(json)
            guard let currentUser = CommonFunction.getUser() else { return }
            print_debug(json[ApiKeys.userImage].stringValue)
             currentUser.userImage = json[ApiKeys.userImage].stringValue
            CommonFunction.updateUser(currentUser)
            self.delegate?.reloadViewsData()
            
        }) { (error) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}

// MARK:-  Image Picker Delegate
//==================================
 extension ChangeProfilePicVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerEditedImage] as? UIImage{
            //  if  imagePickerOpenFor == ImagePickerOpenFor.UserPhoto{
            
           // guard let cell = self.registerTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfilePictureCell else { return }
            self.profileImageView.image = imagePicked
//            cell.profileImage.contentMode = .scaleAspectFill
//            cell.profileImage.clipsToBounds = true
            self.uploadImageToS3(image: imagePicked)
            //  self.pickedImage = imagePicked
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func uploadImageToS3(image: UIImage){
        
       // guard let cell = self.registerTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfilePictureCell else { return }
        
        activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            
            if uploaded {
                
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
                self.updateImage(imageUrl: imageUrl)
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, size: {
                    (_) in
        }, failure: { (err : Error) in
            CommonFunction.showToast(msg: err.localizedDescription)
            self.activityIndicator.stopAnimating()
            self.view.isUserInteractionEnabled = true
        })
    }
}
